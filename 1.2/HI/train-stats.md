Statistics HI
=============
### train.cupt
* Sentences: 282
* Tokens: 5764
* Total VMWEs: 175
  * `LVC.cause`: 3
  * `LVC.full`: 109
  * `MVC`: 52
  * `VID`: 11

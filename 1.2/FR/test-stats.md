Statistics FR
=============
### test.cupt
* Sentences: 5011
* Tokens: 126420
* Total VMWEs: 1359
  * `IRV`: 347
  * `LVC.cause`: 22
  * `LVC.full`: 481
  * `MVC`: 4
  * `VID`: 505
* Unseen w.r.t. train: 312
* Ratio of unseen w.r.t. train: 0.22958057395143489
* Unseen w.r.t. train+dev: 300
* Ratio of unseen w.r.t. train+dev: 0.22075055187637968

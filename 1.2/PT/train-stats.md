Statistics PT
=============
### train.cupt
* Sentences: 23905
* Tokens: 542497
* Total VMWEs: 4777
  * `IRV`: 763
  * `LVC.cause`: 98
  * `LVC.full`: 2960
  * `MVC`: 11
  * `VID`: 945

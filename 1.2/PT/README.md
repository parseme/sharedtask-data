README
------
This is the README file for the PARSEME verbal multiword expressions (VMWEs) corpus for Portuguese, edition 1.2. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present data is an update and an extension of the Portuguese corpus of the [PARSEME 1.1 shared task](http://hdl.handle.net/11372/LRT-2842). 
Changes with respect to the 1.1 version are the following:
* We have annotated a new set of sentences, corresponding to the Brazilian Portuguese part of the 2.5 release of the [UD Bosque treebank](https://github.com/UniversalDependencies/UD_Portuguese-Bosque/)
* We have updated the morphosyntactic annotations of the `DG` and `GSD` subcorpora to UD2.5 release by running a parser trained on Bosque (details below)
* We have updated the annotations using the consistency checks tools of PARSEME

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Source corpora
-------
All annotated data come from one of these sources (subcorpora):
1. `DG`: 19,040 sentences from the [Diário Gaúcho](http://diariogaucho.clicrbs.com.br/rs/) regional newspaper, collected by the [PorPopular project](http://www.ufrgs.br/textecc/porlexbras/porpopular/) team and provided by project members through personal communication.
2. `GSD`: 9,664 sentences from the training part of the Universal Dependencies v2.5 treebank for Brazilian Portuguese [UD_Portuguese-GSD](https://github.com/UniversalDependencies/UD_Portuguese-GSD/) (previously called UD_Portuguese-BR), contaning web-crawled texts.
3. `Bosque`: 4,213 sentences from the Brazilian Portuguese subpart (CETENFolha) of the Universal Dependencies v2.5 treebank for Brazilian Portuguese [UD_Portuguese-Bosque](https://github.com/UniversalDependencies/UD_Portuguese-Bosque/), containing newspaper texts.


Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* FORM (column 2): Available. Provided in the original corpora and manually corrected for token _en_ when in contractions: see [this issue](https://github.com/UniversalDependencies/UD_Portuguese-GSD/issues/9)
* LEMMA (column 3): Available. Manually provided in `Bosque`. Automatically generated using UDPipe for `DG`. In `GSD`, a hybrid strategy was used: some lemmas provided in the UD 2.5 release were kept, while the missing lemmas were generated using UDPipe.
* UPOS (column 4): Available. Manually annotated for `Bosque` and `GSD`, automatically provided using UDPipe for `DG`. Uses UD v2 tagset.
* XPOS (column 5): Partly available. Not provided in `DG`, copy of UPOS in `GSD` and original tags from Floresta Sintá(c)tica in `Bosque`, as in the workbench version of the UD treebank.
* FEATS (column 6): Manually provided in `Bosque`. Automatically generated using UDPipe for `DG`. In `GSD`, a hybrid strategy was used: some feats provided in the UD 2.5 release were kept, while the missing feats were generated using UDPipe. Uses UD v2 tagset.
* HEAD and DEPREL (columns 7 and 8): Available. Manually annotated for `Bosque` and `GSD`, automatically provided by UDPipe for `DG`. Uses UD v2 tagset.
* MISC (column 10): No-space information available. Manually annotated for `GSD` and `Bosque`, automatically provided by UDPipe for `DG`.
* PARSEME:MWE (column 11): Manually annotated by a single annotator per file. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: VID, MVC, LVC.full, LVC.cause, IRV.

The UDPipe annotation relied on the model [portuguese-bosque-ud-2.5-191206.udpipe](https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/portuguese-bosque-ud-2.5-191206.udpipe?sequence=75&isAllowed=y) trained on Portuguese Bosque UD treebank, release 2.5.


Companion raw corpus
--------------------
The manually annotated corpus, described above, is accompanied by a large "raw" corpus (meant for automatic discovery of VMWEs), in which VMWEs are not annotated and morphosyntax is automatically tagged. Its characteristics are the following:
* size (uncompressed): 19Gb
* sentences: 26042884
* tokens: 306603890
* tokens/sentence: 11.77
* format: [CoNLL-U](https://universaldependencies.org/format.html)
* source: 
  * files `raw-001.conllu.gz` through `raw-018.conllu.gz` correspond to the Wikipedia dump provided in the [raw corpora of the CoNLL 2017 shared task](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-1989). The files were re-parsed using UDPipe, as described below. 
  * file `raw-019.conllu.gz` corresponds to articles from the Folha de São Paulo newspaper provided by Oto Vale. The files were parsed using UDPipe, as described below.
* genre: Wikipedia pages and newspaper (Folha)
* morpho-syntactic tagging: upgraded to a [UD-2.5](http://hdl.handle.net/11234/1-3105)-compatible version with [UDPipe](http://ufal.mff.cuni.cz/udpipe) using the [portuguese-bosque-ud-2.5-191206.udpipe](https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/portuguese-bosque-ud-2.5-191206.udpipe?sequence=75&isAllowed=y) model (same as for the automatically tagged parts of the manually annotated corpus)
* compatibility of the raw corpus with the manually annotated corpus: same tagset and UdPipe model used.

Tokenization
------------
* We have re-tokenized `DG` using UDPipe's tokenizer trained on `Bosque`.
* For `Bosque` and `GSD`, we used the reference tokenization provided in the UD 2.5 release (which has some problems, as mentioned below)
* In both corpora, contractions are split, but `DG` may contain errors due to automatic processing whereas `GSD` does not represent reflexive verbs as ranges (see issues below).

Known issues
------------
* Ranges for `IRV`s: `GSD` and `Bosque` are not uniform in their treament of contractions. This problem should be fixed on the original `GSD` treebank and then projected onto the PARSEME corpus in the future. See [this issue](https://github.com/UniversalDependencies/UD_Portuguese-GSD/issues/10)
* Lemmas: The quality of the automatic lemmatizer is limited because it was learned on a small treebank and not checked using dictionaries. See [this issue](https://github.com/UniversalDependencies/UD_Portuguese-GSD/issues/8) for `GSD`
* Merging _ea_ and _eo_: The UD corpus strangely merges _e_+_a_ (_and_+_the.FEM_) and _e_+_o_ (_and_+_the.MASC_) at some places.  We have left the surface untouched (as "ea" and "eo"), to be able to keep the same tokenization as in the original corpus. See [this issue](https://github.com/UniversalDependencies/UD_Portuguese-GSD/issues/5)
* Contractions: some contractions are not dealt with in the same way in `Bosque` and `GSD`. See [this issue](https://github.com/UniversalDependencies/UD_Portuguese-GSD/issues/14)
* Some annotation problems were found in the [PBML ambiguity paper](https://ufal.mff.cuni.cz/pbml/112/) and the [corresponding dataset](http://hdl.handle.net/11372/LRT-2966) but those were not corrected in this release for lack of time.


License
-------
The full dataset is licensed under **Creative Commons Non-Commercial Share-Alike 4.0** licence [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Authors
-------
For version 1.2: Renata Ramisch, Carlos Ramisch, Isaac Miranda, Alexandre Rademaker, Oto Vale, Aline Villavicencio, Gabriela Wick Pedro, Rodrigo Wilkens, Leonardo Zilio.
For version 1.1: Silvio Cordeiro, Carlos Ramisch, Renata Ramisch, Leonardo Zilio.
For version 1.0: Helena Caseli, Silvio Cordeiro, Carlos Ramisch, Renata Ramisch, Aline Villavicencio, Leonardo Zilio.

Contact
-------
* Corpus processing: `carlos.ramisch@lis-lab.fr`
* VMWE annotations: `renata.ramisch@gmail.com`


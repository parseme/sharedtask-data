Statistics PT
=============
### test.cupt
* Sentences: 6236
* Tokens: 142377
* Total VMWEs: 1263
  * `IRV`: 191
  * `LVC.cause`: 23
  * `LVC.full`: 763
  * `MVC`: 5
  * `VID`: 281
* Unseen w.r.t. train: 319
* Ratio of unseen w.r.t. train: 0.25257323832145684
* Unseen w.r.t. train+dev: 300
* Ratio of unseen w.r.t. train+dev: 0.2375296912114014

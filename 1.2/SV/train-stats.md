Statistics SV
=============
### train.cupt
* Sentences: 1605
* Tokens: 24970
* Total VMWEs: 752
  * `IRV`: 41
  * `LVC.cause`: 6
  * `LVC.full`: 95
  * `VID`: 105
  * `VPC.full`: 345
  * `VPC.semi`: 160

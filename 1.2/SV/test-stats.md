Statistics SV
=============
### test.cupt
* Sentences: 2103
* Tokens: 31623
* Total VMWEs: 969
  * `IRV`: 50
  * `LVC.cause`: 5
  * `LVC.full`: 142
  * `VID`: 146
  * `VPC.full`: 418
  * `VPC.semi`: 208
* Unseen w.r.t. train: 357
* Ratio of unseen w.r.t. train: 0.3684210526315789
* Unseen w.r.t. train+dev: 300
* Ratio of unseen w.r.t. train+dev: 0.30959752321981426

Statistics HE
=============
### train.cupt
* Sentences: 14152
* Tokens: 286262
* Total VMWEs: 1864
  * `LVC.cause`: 166
  * `LVC.full`: 765
  * `VID`: 825
  * `VPC.full`: 108

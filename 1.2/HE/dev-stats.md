Statistics HE
=============
### dev.cupt
* Sentences: 1254
* Tokens: 25392
* Total VMWEs: 166
  * `LVC.cause`: 13
  * `LVC.full`: 80
  * `VID`: 64
  * `VPC.full`: 9

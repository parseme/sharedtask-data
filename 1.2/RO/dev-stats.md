Statistics RO
=============
### dev.cupt
* Sentences: 7714
* Tokens: 134340
* Total VMWEs: 818
  * `IRV`: 504
  * `LVC.cause`: 30
  * `LVC.full`: 56
  * `VID`: 228

README
======

This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Italian, edition 1.2.

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Corpora
-------
All annotated data comes from the [PAISÀ Corpus](http://www.corpusitaliano.it/en/) converted to [CoNLL-U format](https://universaldependencies.org/format.html) version 2. Differently from edition 1.1 the new conversion makes sure that **UPOS**, **FEATS** and **DEPREL** are consistent to CoNLL-U format.

The original corpus is distributed under Creative Commons license, Attribution-ShareAlike and Attribution-Noncommercial-ShareAlike.

There are two types of corpora: annotated and raw.

### Annotated Corpus (cupt)

The Annotated Corpus uses the [.cupt](http://multiword.sourceforge.net/cupt-format) format. It includes the same set of annotated sentences used in the ST 1.1 (with identical MWE annotation.

The corpus includes sentences extracted from the following sections:

|Section     |Sentences|
|------------|--------:|
| blogolandia|4,728    |
| wikinews   |5,000    |
| wikipedia  |6,000    |
| TOTAL      |15,728   |

<!-- The sentences preserve the same **tain**, **dev**, **test** split as in ST 1.1:

|Section     |Sentences|
|------------|--------:|
| train      |13,555   |
| dev        |917      |
| test       |1,256    |
| TOTAL      |15,728   | 
-->

### Raw Corpus (CoNNL-U)
This corpus consists of the entire PAISÀ Corpus converted to CoNLL-U format, except for the sentences present in the annotated corpus and few faulty sentences (those containing invalid HEAD fields).

|     |Sentences|
|------------|--------:|
| Total sentences in PAISÀ      |12,297,010   |
| Discarded (annotated corpus)       |- 15,728    |
| Discarded (faulty)        |- 609      |
| **Total in Raw Corpus**      |12,280,673   |




Provided annotations
--------------------
Both **Annotated Corpus** and **Raw Corpus** are obtained from the same conversion script, so they share the same UD tagset (version 2). They have in common the first 10 columns of the CoNLL-U format.

|COLUMN|FIELD|DESCRITPION|ANNOTATION|
|------|-----|-----------|----------|
| 1 | ID | Word index, integer starting at 1 for each new sentence; may be a range for multiword tokens.
| 2 | FORM | Word form or punctuation symbol.
| 3 | LEMMA | Lemma or stem of word form. | Automatically annotated (as in the original corpus).
| 4 | UPOS | [Universal part-of-speech tag](https://universaldependencies.org/u/pos/index.html). | **Converted** from [original automatically annotated corpus](http://www.corpusitaliano.it/en/contents/description.html).
| 5 | XPOS | Language-specific part-of-speech tag; underscore if not available. | Automatically annotated (fined graned postag in [original automatically annotated corpus](http://www.corpusitaliano.it/en/contents/description.html)). 
| 6 | FEATS | List of morphological features from the [universal feature inventory](https://universaldependencies.org/u/feat/index.html); underscore if not available. | **Converted** from [original automatically annotated corpus](http://www.corpusitaliano.it/en/contents/description.html).
| 7 | HEAD | Head of the current word, which is either a value of ID or zero (0). | Automatically annotated (as in the original corpus).
| 8 | DEPREL | Universal dependency relation to the HEAD (root iff HEAD = 0) or a defined language-specific subtype of one.| **Converted** from [original automatically annotated corpus](http://www.corpusitaliano.it/en/contents/description.html).
| 9 | DEPS | Enhanced dependency graph in the form of a list of head-deprel pairs. | Not used (always set to underscore).
| 10 | MISC | Any other annotation. | Automatic annotation of *SpaceAfter=No* for every token after which there was no space in the original text.

In addition, the **Annotated** (cupt) format
has an additional 11th field:

| | | |
|----|-----|--------|
| 11 | PARSEME:MWE |Manually annotated (no changes wrt to ST 1.1). VMWEs in this language have been annotated by a single annotator per file. The following categories are used (http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/):  LVC.full, LVC.cause, VID, VPC.full, VPC.semi, IRV, IAV, MVC, LS.ICV. 


Tokenization
-------------
The tokenization follows the original tokenization of the PAISÀ corpus with the exception of compound prepositions. 

Some pre-processing has been applied to the original files of the corpus in order to split compound prepositions (dei, nei, delle, etc.).
To this end we added new tokens corresponding to the components of the compound prepositions (see example below)  and we also realigned all the dependency index: the heuristic being used is that the preposition is the head of the prepositional article (all tokens pointing to the prepositional article will point to the preposition in the split version and the determiner also points to the preposition).

SpaceAfter=No
-------------
This tag is present in the `MISC` field. 
It has been automatic annotated based on a set of rules capturing the use of spaces before/after puntuation symbols. The only symbol that doesn't have a consistent annotation is the quotation mark ("), which in some context it's ambiguous if it is an open or close quotation mark (i.e., when there are an odd number in the same sentence).

In addition we also introduced this tag on the word preceding a clitic beloging to the same token, e.g., lavar- si. These are often annotated as two separate words in the the original corpus. In these cases, we also removed the trailing dash (-) in the first token.

Conversion Example
------------------

Here an example sentence converted from the original [PAISÀ CoNLL format]((http://www.corpusitaliano.it/en/contents/description.html) to the CoNLL-U format.

<!-- (text_id:0000315_31) -->

Orignal annotated sentence:

|ID | FORM | LEMMA | CPOSTAG | POSTAG | FEATS | HEAD | DEPREL
|----|----|---|---|---|---|---|---|
1 | Solo | solo | B | B | _ | 2 | mod
2 | intorno | intorno | B | B | _ | 6 | mod
3 | all' | al | E | EA | num=s \| gen=n | 6 | comp_loc
4 | Ottocento | Ottocento | S | SP | _ | 3 | prep
5 | Agropoli | Agropoli | S | SP | _ | 4 | mod
6 | cominciò | cominciare | V | V | num=s \| per=3 \| mod=i \| ten=s | 0 | ROOT
7 | ad | ad | E | E | _ | 6 | arg
8 | avere | avere | V | V | mod=f | 7 | prep
9 | una | una | R | RI | num=s \| gen=f | 11 | det
10 | vasta | vasto | A | A | num=s \| gen=f | 11 | mod
11 | espansione | espansione | S | S | num=s \| gen=f | 8 | obj
12 | , | , | F | FF | _ | 13 | punc
13 | allargando- | allargare | V | V | mod=g | 8 | mod
14 | si | si | P | PC | num=n \| per=3 \| gen=n | 13 | clit
15 | oltre | oltre | E | E | _ | 13 | comp
16 | l' | il | R | RD | num=s \| gen=n | 18 | det
17 | antico | antico | A | A | num=s \| gen=m | 18 | mod
18 | borgo | borgo | S | S | num=s \| gen=m | 15 | prep
19 | . | . | F | FS | _ | 6 | punc

Converted sentene:

ID | FORM | LEMMA | UPOS | XPOS | FEATS | HEAD | DEPREL | DEPS | MISC
|---|---|---|---|---|---|---|---|---|---|
1 | Solo | solo | ADV | B | _ | 2 | advmod | _ | _
2 | intorno | intorno | ADV | B | _ | 7 | advmod | _ | _
3-4 | all' | _ | _ | _ | _ | _ | _ | _ | SpaceAfter=No
3 | a | a | ADP | E | _ | 7 | nmod | _ | _
4 | l' | il | DET | RD | Definite=Def \| PronType=Art | 3 | det | _ | _
5 | Ottocento | Ottocento | PROPN | SP | _ | 3 | case | _ | _
6 | Agropoli | Agropoli | PROPN | SP | _ | 5 | nmod | _ | _
7 | cominciò | cominciare | VERB | V | Mood=Ind \| Number=Sing \| Person=3 \| Tense=Past | 0 | ROOT | _ | _
8 | ad | ad | ADP | E | _ | 7 | mark | _ | _
9 | avere | avere | VERB | V | VerbForm=Inf | 8 | mark | _ | _
10 | una | una | DET | RI | Definite=Ind \| Gender=Fem \| Number=Sing \| PronType=Art | 12 | det | _ | _
11 | vasta | vasto | ADJ | A | Gender=Fem \| Number=Sing | 12 | amod | _ | _
12 | espansione | espansione | NOUN | S | Gender=Fem \| Number=Sing | 9 | obj | _ | SpaceAfter=No
13 | , | , | PUNCT | FF | _ | 14 | punct | _ | _
14 | allargando | allargare | VERB | V | VerbForm=Ger | 9 | advcl | _ | SpaceAfter=No
15 | si | si | PRON | PC | Person=3 \| PronType=Clit | 14 | expl | _ | _
16 | oltre | oltre | ADP | E | _ | 14 | nmod | _ | _
17 | l' | il | DET | RD | Definite=Def \| Number=Sing \| PronType=Art | 19 | det | _ | SpaceAfter=No
18 | antico | antico | ADJ | A | Gender=Masc \| Number=Sing | 19 | amod | _ | _
19 | borgo | borgo | NOUN | S | Gender=Masc \| Number=Sing | 16 | case | _ | SpaceAfter=No
20 | . | . | PUNCT | FS | _ | 7 | punct | _ | _

Licence
-------
The full dataset is licensed under Creative Commons Non-Commercial Share-Alike 4.0 licence CC-BY-NC-SA 4.0.

Authors
-------
Conceptual mapping and converter development: Johanna Monti, Carola Carlino, Maria Pia di Buono, Federico Sangati, Giulia Speranza
VMWE annotation: Johanna Monti, Valeria Caruso, Maria Pia di Buono, Antonio Pascucci, Annalisa Raffone, Anna Riccio, Federico Sangati.

Contact
-------
jmonti@unior.it or federico.sangati@gmail.com

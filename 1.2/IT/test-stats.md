Statistics IT
=============
### test.cupt
* Sentences: 3885
* Tokens: 106072
* Total VMWEs: 1032
  * `IAV`: 110
  * `IRV`: 280
  * `LS.ICV`: 12
  * `LVC.cause`: 44
  * `LVC.full`: 180
  * `MVC`: 10
  * `VID`: 376
  * `VPC.full`: 20
* Unseen w.r.t. train: 320
* Ratio of unseen w.r.t. train: 0.31007751937984496
* Unseen w.r.t. train+dev: 300
* Ratio of unseen w.r.t. train+dev: 0.29069767441860467

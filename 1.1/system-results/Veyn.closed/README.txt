Veyn
Nicolas Zampieri, Manon Scholivet, Carlos Ramisch and Benoit Favre
(Submission #9)

Summary
Our system is based on a sequence tagger using Recurrent Neural Networks (RNNs). We represent VMWEs using a Begin-Insie-Outside (BIO) encoding scheme combined with the VMWE category tag. The label B is assigned to a token at the beginning of an expression, I is assigned to a non-initial token belonging to an expression, and O is used if the token is outside an expression. A special label (lowercase o) is used for tokens not belonging to an expression, but occurring in between words that belong to an expression. Each B and I tags are concatenated with the provided VMWE's category (IRV, LVC.full, VID, etc). The goal of the RNN is to predict the right tag for each token.

The model uses LEMMA and UPOS as input (falling back to FORM and XPOS, respectively, if the former are absent). Each token's LEMMA and UPOS are converted into one-hot vectors, which are then concatenated and transformed into embeddings. Input lemma and POS embeddings are pre-initialized on the shared task training corpora, but tuned during the training phase. These embeddings are then forwarded to a double bidirectional GRU layer. Finally, each the prediction of a label is based on a softmax layer that takes as input the concatenation of the GRU cell outputs in both directions for each token. The VMWEs annotations are then reconstructed based on heuristic rules that group together B+I tokens with the same category. All parameters other than the input are initialized randomly. The RNN is implemented using the python Keras library.

We used "train.cupt" files to train our models for all languages, "dev.cupt" to tune some parameters such as the optimal tag encoding scheme, and "test.blind.cupt" to obtain the predicted results for submission.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)

## Global evaluation
* MWE-based: P=87/372=0.2339 R=87/502=0.1733 F=0.1991
* Tok-based: P=275/700=0.3929 R=275/1155=0.2381 F=0.2965

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=49/502=10% pred=36/372=10%
* LVC.cause: MWE-based: P=1/36=0.0278 R=1/49=0.0204 F=0.0235
* LVC.cause: Tok-based: P=4/51=0.0784 R=4/101=0.0396 F=0.0526
* LVC.full: MWE-proportion: gold=211/502=42% pred=268/372=72%
* LVC.full: MWE-based: P=37/268=0.1381 R=37/211=0.1754 F=0.1545
* LVC.full: Tok-based: P=107/505=0.2119 R=107/435=0.2460 F=0.2277
* VID: MWE-proportion: gold=182/502=36% pred=68/372=18%
* VID: MWE-based: P=8/68=0.1176 R=8/182=0.0440 F=0.0640
* VID: Tok-based: P=33/144=0.2292 R=33/467=0.0707 F=0.1080
* VPC.full: MWE-proportion: gold=60/502=12% pred=0/372=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/60=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/152=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=384/502=76% pred=318/372=85%
* Continuous: MWE-based: P=71/318=0.2233 R=71/384=0.1849 F=0.2023
* Discontinuous: MWE-proportion: gold=118/502=24% pred=54/372=15%
* Discontinuous: MWE-based: P=16/54=0.2963 R=16/118=0.1356 F=0.1860

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/502=100% pred=288/372=77%
* Multi-token: MWE-based: P=87/288=0.3021 R=87/502=0.1733 F=0.2203

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=174/502=35% pred=90/372=24%
* Seen-in-train: MWE-based: P=76/90=0.8444 R=76/174=0.4368 F=0.5758
* Unseen-in-train: MWE-proportion: gold=328/502=65% pred=282/372=76%
* Unseen-in-train: MWE-based: P=11/282=0.0390 R=11/328=0.0335 F=0.0361

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=72/174=41% pred=44/90=49%
* Variant-of-train: MWE-based: P=30/44=0.6818 R=30/72=0.4167 F=0.5172
* Identical-to-train: MWE-proportion: gold=102/174=59% pred=46/90=51%
* Identical-to-train: MWE-based: P=46/46=1.0000 R=46/102=0.4510 F=0.6216


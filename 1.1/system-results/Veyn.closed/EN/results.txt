## Global evaluation
* MWE-based: P=15/54=0.2778 R=15/501=0.0299 F=0.0541
* Tok-based: P=37/91=0.4066 R=37/1087=0.0340 F=0.0628

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=44/501=9% pred=0/54=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/44=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/98=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=36/501=7% pred=0/54=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/36=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/72=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=166/501=33% pred=0/54=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/166=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/340=0.0000 F=0.0000
* MVC: MWE-proportion: gold=4/501=1% pred=0/54=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* VID: MWE-proportion: gold=79/501=16% pred=0/54=0%
* VID: MWE-based: P=0/0=0.0000 R=0/79=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/225=0.0000 F=0.0000
* VPC.full: MWE-proportion: gold=146/501=29% pred=54/54=100%
* VPC.full: MWE-based: P=13/54=0.2407 R=13/146=0.0890 F=0.1300
* VPC.full: Tok-based: P=30/91=0.3297 R=30/292=0.1027 F=0.1567
* VPC.semi: MWE-proportion: gold=26/501=5% pred=0/54=0%
* VPC.semi: MWE-based: P=0/0=0.0000 R=0/26=0.0000 F=0.0000
* VPC.semi: Tok-based: P=0/0=0.0000 R=0/52=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=295/501=59% pred=54/54=100%
* Continuous: MWE-based: P=15/54=0.2778 R=15/295=0.0508 F=0.0860
* Discontinuous: MWE-proportion: gold=206/501=41% pred=0/54=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/206=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/501=99% pred=36/54=67%
* Multi-token: MWE-based: P=15/36=0.4167 R=15/498=0.0301 F=0.0562
* Single-token: MWE-proportion: gold=3/501=1% pred=18/54=33%
* Single-token: MWE-based: P=0/18=0.0000 R=0/3=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=143/501=29% pred=17/54=31%
* Seen-in-train: MWE-based: P=9/17=0.5294 R=9/143=0.0629 F=0.1125
* Unseen-in-train: MWE-proportion: gold=358/501=71% pred=37/54=69%
* Unseen-in-train: MWE-based: P=6/37=0.1622 R=6/358=0.0168 F=0.0304

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=76/143=53% pred=10/17=59%
* Variant-of-train: MWE-based: P=3/10=0.3000 R=3/76=0.0395 F=0.0698
* Identical-to-train: MWE-proportion: gold=67/143=47% pred=7/17=41%
* Identical-to-train: MWE-based: P=6/7=0.8571 R=6/67=0.0896 F=0.1622


## Global evaluation
* MWE-based: P=290/397=0.7305 R=290/515=0.5631 F=0.6360
* Tok-based: P=633/798=0.7932 R=633/1108=0.5713 F=0.6642

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=29/515=6% pred=17/397=4%
* IAV: MWE-based: P=16/17=0.9412 R=16/29=0.5517 F=0.6957
* IAV: Tok-based: P=37/37=1.0000 R=37/64=0.5781 F=0.7327
* IRV: MWE-proportion: gold=249/515=48% pred=255/397=64%
* IRV: MWE-based: P=194/255=0.7608 R=194/249=0.7791 F=0.7698
* IRV: Tok-based: P=400/500=0.8000 R=400/499=0.8016 F=0.8008
* LVC.cause: MWE-proportion: gold=15/515=3% pred=8/397=2%
* LVC.cause: MWE-based: P=2/8=0.2500 R=2/15=0.1333 F=0.1739
* LVC.cause: Tok-based: P=4/12=0.3333 R=4/33=0.1212 F=0.1778
* LVC.full: MWE-proportion: gold=149/515=29% pred=92/397=23%
* LVC.full: MWE-based: P=62/92=0.6739 R=62/149=0.4161 F=0.5145
* LVC.full: Tok-based: P=136/184=0.7391 R=136/305=0.4459 F=0.5562
* VID: MWE-proportion: gold=73/515=14% pred=25/397=6%
* VID: MWE-based: P=15/25=0.6000 R=15/73=0.2055 F=0.3061
* VID: Tok-based: P=49/65=0.7538 R=49/207=0.2367 F=0.3603

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=361/515=70% pred=328/397=83%
* Continuous: MWE-based: P=244/328=0.7439 R=244/361=0.6759 F=0.7083
* Discontinuous: MWE-proportion: gold=154/515=30% pred=69/397=17%
* Discontinuous: MWE-based: P=46/69=0.6667 R=46/154=0.2987 F=0.4126

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=515/515=100% pred=373/397=94%
* Multi-token: MWE-based: P=290/373=0.7775 R=290/515=0.5631 F=0.6532

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=371/515=72% pred=284/397=72%
* Seen-in-train: MWE-based: P=267/284=0.9401 R=267/371=0.7197 F=0.8153
* Unseen-in-train: MWE-proportion: gold=144/515=28% pred=113/397=28%
* Unseen-in-train: MWE-based: P=23/113=0.2035 R=23/144=0.1597 F=0.1790

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=223/371=60% pred=137/284=48%
* Variant-of-train: MWE-based: P=127/137=0.9270 R=127/223=0.5695 F=0.7056
* Identical-to-train: MWE-proportion: gold=148/371=40% pred=147/284=52%
* Identical-to-train: MWE-based: P=140/147=0.9524 R=140/148=0.9459 F=0.9492


## Global evaluation
* MWE-based: P=244/771=0.3165 R=244/500=0.4880 F=0.3839
* Tok-based: P=616/1607=0.3833 R=616/1150=0.5357 F=0.4469

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=64/500=13% pred=120/771=16%
* IAV: MWE-based: P=34/120=0.2833 R=34/64=0.5312 F=0.3696
* IAV: Tok-based: P=81/257=0.3152 R=81/148=0.5473 F=0.4000
* IRV: MWE-proportion: gold=121/500=24% pred=234/771=30%
* IRV: MWE-based: P=81/234=0.3462 R=81/121=0.6694 F=0.4563
* IRV: Tok-based: P=164/459=0.3573 R=164/242=0.6777 F=0.4679
* LVC.cause: MWE-proportion: gold=28/500=6% pred=6/771=1%
* LVC.cause: MWE-based: P=1/6=0.1667 R=1/28=0.0357 F=0.0588
* LVC.cause: Tok-based: P=3/15=0.2000 R=3/63=0.0476 F=0.0769
* LVC.full: MWE-proportion: gold=85/500=17% pred=114/771=15%
* LVC.full: MWE-based: P=22/114=0.1930 R=22/85=0.2588 F=0.2211
* LVC.full: Tok-based: P=58/207=0.2802 R=58/176=0.3295 F=0.3029
* MVC: MWE-proportion: gold=106/500=21% pred=252/771=33%
* MVC: MWE-based: P=62/252=0.2460 R=62/106=0.5849 F=0.3464
* MVC: Tok-based: P=168/554=0.3032 R=168/249=0.6747 F=0.4184
* VID: MWE-proportion: gold=95/500=19% pred=48/771=6%
* VID: MWE-based: P=22/48=0.4583 R=22/95=0.2316 F=0.3077
* VID: Tok-based: P=64/120=0.5333 R=64/270=0.2370 F=0.3282
* VPC.full: MWE-proportion: gold=1/500=0% pred=0/771=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=359/500=72% pred=715/771=93%
* Continuous: MWE-based: P=225/715=0.3147 R=225/359=0.6267 F=0.4190
* Discontinuous: MWE-proportion: gold=141/500=28% pred=56/771=7%
* Discontinuous: MWE-based: P=19/56=0.3393 R=19/141=0.1348 F=0.1929

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=702/771=91%
* Multi-token: MWE-based: P=244/702=0.3476 R=244/500=0.4880 F=0.4060

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=279/500=56% pred=392/771=51%
* Seen-in-train: MWE-based: P=181/392=0.4617 R=181/279=0.6487 F=0.5395
* Unseen-in-train: MWE-proportion: gold=221/500=44% pred=379/771=49%
* Unseen-in-train: MWE-based: P=63/379=0.1662 R=63/221=0.2851 F=0.2100

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=145/279=52% pred=139/392=35%
* Variant-of-train: MWE-based: P=65/139=0.4676 R=65/145=0.4483 F=0.4577
* Identical-to-train: MWE-proportion: gold=134/279=48% pred=253/392=65%
* Identical-to-train: MWE-based: P=116/253=0.4585 R=116/134=0.8657 F=0.5995


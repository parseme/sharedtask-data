## Global evaluation
* MWE-based: P=364/445=0.8180 R=364/500=0.7280 F=0.7704
* Tok-based: P=754/874=0.8627 R=754/1006=0.7495 F=0.8021

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=17/500=3% pred=14/445=3%
* LVC.cause: MWE-based: P=8/14=0.5714 R=8/17=0.4706 F=0.5161
* LVC.cause: Tok-based: P=16/26=0.6154 R=16/34=0.4706 F=0.5333
* LVC.full: MWE-proportion: gold=410/500=82% pred=376/445=84%
* LVC.full: MWE-based: P=311/376=0.8271 R=311/410=0.7585 F=0.7913
* LVC.full: Tok-based: P=640/737=0.8684 R=640/821=0.7795 F=0.8216
* VID: MWE-proportion: gold=73/500=15% pred=55/445=12%
* VID: MWE-based: P=37/55=0.6727 R=37/73=0.5068 F=0.5781
* VID: Tok-based: P=77/111=0.6937 R=77/151=0.5099 F=0.5878

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=407/500=81% pred=401/445=90%
* Continuous: MWE-based: P=337/401=0.8404 R=337/407=0.8280 F=0.8342
* Discontinuous: MWE-proportion: gold=93/500=19% pred=44/445=10%
* Discontinuous: MWE-based: P=27/44=0.6136 R=27/93=0.2903 F=0.3942

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=424/445=95%
* Multi-token: MWE-based: P=364/424=0.8585 R=364/500=0.7280 F=0.7879

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=422/500=84% pred=346/445=78%
* Seen-in-train: MWE-based: P=335/346=0.9682 R=335/422=0.7938 F=0.8724
* Unseen-in-train: MWE-proportion: gold=78/500=16% pred=99/445=22%
* Unseen-in-train: MWE-based: P=29/99=0.2929 R=29/78=0.3718 F=0.3277

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=164/422=39% pred=95/346=27%
* Variant-of-train: MWE-based: P=87/95=0.9158 R=87/164=0.5305 F=0.6718
* Identical-to-train: MWE-proportion: gold=258/422=61% pred=251/346=73%
* Identical-to-train: MWE-based: P=248/251=0.9880 R=248/258=0.9612 F=0.9745


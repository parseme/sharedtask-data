SHOMA
Shiva Taslimipoor and Omid Rohanian
(Submission #19)

Summary
Our sequence tagging system consists of a front-end ConvNet, two stacked LSTMS, and one optional CRF recurrent layer that has been used for some of the tested languages. Our system receives word and POS representations as inputes. POS information (included in the training data) is encoded using one-hot representation. For word embeddings we use vectors pretrained using a variant of word2vec from the following link. https://github.com/facebookresearch/fastText/blob/master/pretrained-vectors.md In the task of multiword expression identification, to our knowledge, this is the first use of CRF within a neural network architecture. We use the CRF layer in cases where we saw improvement in the model's performance when tested on the development set

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	OPEN
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)

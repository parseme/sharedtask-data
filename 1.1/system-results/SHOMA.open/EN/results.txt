## Global evaluation
* MWE-based: P=58/127=0.4567 R=58/501=0.1158 F=0.1847
* Tok-based: P=129/230=0.5609 R=129/1087=0.1187 F=0.1959

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=44/501=9% pred=3/127=2%
* IAV: MWE-based: P=1/3=0.3333 R=1/44=0.0227 F=0.0426
* IAV: Tok-based: P=2/6=0.3333 R=2/98=0.0204 F=0.0385
* LVC.cause: MWE-proportion: gold=36/501=7% pred=0/127=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/36=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/72=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=166/501=33% pred=7/127=6%
* LVC.full: MWE-based: P=2/7=0.2857 R=2/166=0.0120 F=0.0231
* LVC.full: Tok-based: P=5/10=0.5000 R=5/340=0.0147 F=0.0286
* MVC: MWE-proportion: gold=4/501=1% pred=0/127=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* VID: MWE-proportion: gold=79/501=16% pred=12/127=9%
* VID: MWE-based: P=2/12=0.1667 R=2/79=0.0253 F=0.0440
* VID: Tok-based: P=8/15=0.5333 R=8/225=0.0356 F=0.0667
* VPC.full: MWE-proportion: gold=146/501=29% pred=97/127=76%
* VPC.full: MWE-based: P=44/97=0.4536 R=44/146=0.3014 F=0.3621
* VPC.full: Tok-based: P=92/186=0.4946 R=92/292=0.3151 F=0.3849
* VPC.semi: MWE-proportion: gold=26/501=5% pred=8/127=6%
* VPC.semi: MWE-based: P=1/8=0.1250 R=1/26=0.0385 F=0.0588
* VPC.semi: Tok-based: P=3/13=0.2308 R=3/52=0.0577 F=0.0923

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=295/501=59% pred=125/127=98%
* Continuous: MWE-based: P=57/125=0.4560 R=57/295=0.1932 F=0.2714
* Discontinuous: MWE-proportion: gold=206/501=41% pred=2/127=2%
* Discontinuous: MWE-based: P=1/2=0.5000 R=1/206=0.0049 F=0.0096

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/501=99% pred=103/127=81%
* Multi-token: MWE-based: P=58/103=0.5631 R=58/498=0.1165 F=0.1930
* Single-token: MWE-proportion: gold=3/501=1% pred=24/127=19%
* Single-token: MWE-based: P=0/24=0.0000 R=0/3=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=143/501=29% pred=62/127=49%
* Seen-in-train: MWE-based: P=38/62=0.6129 R=38/143=0.2657 F=0.3707
* Unseen-in-train: MWE-proportion: gold=358/501=71% pred=65/127=51%
* Unseen-in-train: MWE-based: P=20/65=0.3077 R=20/358=0.0559 F=0.0946

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=76/143=53% pred=27/62=44%
* Variant-of-train: MWE-based: P=17/27=0.6296 R=17/76=0.2237 F=0.3301
* Identical-to-train: MWE-proportion: gold=67/143=47% pred=35/62=56%
* Identical-to-train: MWE-based: P=21/35=0.6000 R=21/67=0.3134 F=0.4118


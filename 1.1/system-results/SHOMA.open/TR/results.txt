## Global evaluation
* MWE-based: P=232/285=0.8140 R=232/506=0.4585 F=0.5866
* Tok-based: P=498/571=0.8722 R=498/1045=0.4766 F=0.6163

## Per-category evaluation (partition of Global)
* LVC.full: MWE-proportion: gold=272/506=54% pred=154/285=54%
* LVC.full: MWE-based: P=136/154=0.8831 R=136/272=0.5000 F=0.6385
* LVC.full: Tok-based: P=285/307=0.9283 R=285/548=0.5201 F=0.6667
* MVC: MWE-proportion: gold=1/506=0% pred=0/285=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* VID: MWE-proportion: gold=233/506=46% pred=131/285=46%
* VID: MWE-based: P=84/131=0.6412 R=84/233=0.3605 F=0.4615
* VID: Tok-based: P=186/264=0.7045 R=186/495=0.3758 F=0.4901

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=208/506=41% pred=138/285=48%
* Continuous: MWE-based: P=112/138=0.8116 R=112/208=0.5385 F=0.6474
* Discontinuous: MWE-proportion: gold=298/506=59% pred=147/285=52%
* Discontinuous: MWE-based: P=120/147=0.8163 R=120/298=0.4027 F=0.5393

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/506=99% pred=278/285=98%
* Multi-token: MWE-based: P=232/278=0.8345 R=232/502=0.4622 F=0.5949
* Single-token: MWE-proportion: gold=4/506=1% pred=7/285=2%
* Single-token: MWE-based: P=0/7=0.0000 R=0/4=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=126/506=25% pred=100/285=35%
* Seen-in-train: MWE-based: P=90/100=0.9000 R=90/126=0.7143 F=0.7965
* Unseen-in-train: MWE-proportion: gold=380/506=75% pred=185/285=65%
* Unseen-in-train: MWE-based: P=142/185=0.7676 R=142/380=0.3737 F=0.5027

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=75/126=60% pred=47/100=47%
* Variant-of-train: MWE-based: P=42/47=0.8936 R=42/75=0.5600 F=0.6885
* Identical-to-train: MWE-proportion: gold=51/126=40% pred=53/100=53%
* Identical-to-train: MWE-based: P=48/53=0.9057 R=48/51=0.9412 F=0.9231


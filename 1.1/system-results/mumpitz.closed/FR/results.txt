## Global evaluation
* MWE-based: P=167/294=0.5680 R=167/498=0.3353 F=0.4217
* Tok-based: P=455/560=0.8125 R=455/1171=0.3886 F=0.5257

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=108/498=22% pred=0/294=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/108=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/216=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=14/498=3% pred=0/294=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/14=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=160/498=32% pred=0/294=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/160=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/333=0.0000 F=0.0000
* MVC: MWE-proportion: gold=4/498=1% pred=0/294=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* <unlabeled>: MWE-proportion: gold=0/498=0% pred=294/294=100%
* <unlabeled>: MWE-based: P=0/294=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/560=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=212/498=43% pred=0/294=0%
* VID: MWE-based: P=0/0=0.0000 R=0/212=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/586=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=281/498=56% pred=178/294=61%
* Continuous: MWE-based: P=103/178=0.5787 R=103/281=0.3665 F=0.4488
* Discontinuous: MWE-proportion: gold=217/498=44% pred=116/294=39%
* Discontinuous: MWE-based: P=64/116=0.5517 R=64/217=0.2949 F=0.3844

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=238/294=81%
* Multi-token: MWE-based: P=167/238=0.7017 R=167/498=0.3353 F=0.4538

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=251/498=50% pred=164/294=56%
* Seen-in-train: MWE-based: P=147/164=0.8963 R=147/251=0.5857 F=0.7084
* Unseen-in-train: MWE-proportion: gold=247/498=50% pred=130/294=44%
* Unseen-in-train: MWE-based: P=20/130=0.1538 R=20/247=0.0810 F=0.1061

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=126/251=50% pred=65/164=40%
* Variant-of-train: MWE-based: P=59/65=0.9077 R=59/126=0.4683 F=0.6178
* Identical-to-train: MWE-proportion: gold=125/251=50% pred=99/164=60%
* Identical-to-train: MWE-based: P=88/99=0.8889 R=88/125=0.7040 F=0.7857


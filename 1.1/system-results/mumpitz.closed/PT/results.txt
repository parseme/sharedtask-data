## Global evaluation
* MWE-based: P=261/583=0.4477 R=261/553=0.4720 F=0.4595
* Tok-based: P=653/1021=0.6396 R=653/1247=0.5237 F=0.5758

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=91/553=16% pred=0/583=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/91=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/184=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=7/553=1% pred=0/583=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/7=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/15=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=337/553=61% pred=0/583=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/337=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/691=0.0000 F=0.0000
* <unlabeled>: MWE-proportion: gold=0/553=0% pred=583/583=100%
* <unlabeled>: MWE-based: P=0/583=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/1021=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=118/553=21% pred=0/583=0%
* VID: MWE-based: P=0/0=0.0000 R=0/118=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/357=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=313/553=57% pred=370/583=63%
* Continuous: MWE-based: P=145/370=0.3919 R=145/313=0.4633 F=0.4246
* Discontinuous: MWE-proportion: gold=240/553=43% pred=213/583=37%
* Discontinuous: MWE-based: P=116/213=0.5446 R=116/240=0.4833 F=0.5121

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=553/553=100% pred=436/583=75%
* Multi-token: MWE-based: P=261/436=0.5986 R=261/553=0.4720 F=0.5278

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=397/553=72% pred=221/583=38%
* Seen-in-train: MWE-based: P=206/221=0.9321 R=206/397=0.5189 F=0.6667
* Unseen-in-train: MWE-proportion: gold=156/553=28% pred=362/583=62%
* Unseen-in-train: MWE-based: P=55/362=0.1519 R=55/156=0.3526 F=0.2124

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=233/397=59% pred=127/221=57%
* Variant-of-train: MWE-based: P=114/127=0.8976 R=114/233=0.4893 F=0.6333
* Identical-to-train: MWE-proportion: gold=164/397=41% pred=94/221=43%
* Identical-to-train: MWE-based: P=92/94=0.9787 R=92/164=0.5610 F=0.7132


## Global evaluation
* MWE-based: P=89/123=0.7236 R=89/496=0.1794 F=0.2876
* Tok-based: P=283/327=0.8654 R=283/1292=0.2190 F=0.3496

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=41/496=8% pred=0/123=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/41=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/101=0.0000 F=0.0000
* IRV: MWE-proportion: gold=96/496=19% pred=0/123=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/96=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/192=0.0000 F=0.0000
* LS.ICV: MWE-proportion: gold=8/496=2% pred=0/123=0%
* LS.ICV: MWE-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* LS.ICV: Tok-based: P=0/0=0.0000 R=0/17=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=25/496=5% pred=0/123=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/25=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/53=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=104/496=21% pred=0/123=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/104=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/231=0.0000 F=0.0000
* MVC: MWE-proportion: gold=5/496=1% pred=0/123=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/5=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/11=0.0000 F=0.0000
* VID: MWE-proportion: gold=201/496=41% pred=123/123=100%
* VID: MWE-based: P=49/123=0.3984 R=49/201=0.2438 F=0.3025
* VID: Tok-based: P=171/327=0.5229 R=171/652=0.2623 F=0.3493
* VPC.full: MWE-proportion: gold=23/496=5% pred=0/123=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/23=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/50=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=331/496=67% pred=120/123=98%
* Continuous: MWE-based: P=87/120=0.7250 R=87/331=0.2628 F=0.3858
* Discontinuous: MWE-proportion: gold=165/496=33% pred=3/123=2%
* Discontinuous: MWE-based: P=2/3=0.6667 R=2/165=0.0121 F=0.0238

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=496/496=100% pred=123/123=100%
* Multi-token: MWE-based: P=89/123=0.7236 R=89/496=0.1794 F=0.2876

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=297/496=60% pred=89/123=72%
* Seen-in-train: MWE-based: P=82/89=0.9213 R=82/297=0.2761 F=0.4249
* Unseen-in-train: MWE-proportion: gold=199/496=40% pred=34/123=28%
* Unseen-in-train: MWE-based: P=7/34=0.2059 R=7/199=0.0352 F=0.0601

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=184/297=62% pred=53/89=60%
* Variant-of-train: MWE-based: P=48/53=0.9057 R=48/184=0.2609 F=0.4051
* Identical-to-train: MWE-proportion: gold=113/297=38% pred=36/89=40%
* Identical-to-train: MWE-based: P=34/36=0.9444 R=34/113=0.3009 F=0.4564


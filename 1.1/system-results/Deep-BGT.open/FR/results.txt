## Global evaluation
* MWE-based: P=248/429=0.5781 R=248/498=0.4980 F=0.5351
* Tok-based: P=661/838=0.7888 R=661/1171=0.5645 F=0.6580

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=108/498=22% pred=108/429=25%
* IRV: MWE-based: P=85/108=0.7870 R=85/108=0.7870 F=0.7870
* IRV: Tok-based: P=171/215=0.7953 R=171/216=0.7917 F=0.7935
* LVC.cause: MWE-proportion: gold=14/498=3% pred=0/429=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/14=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=160/498=32% pred=130/429=30%
* LVC.full: MWE-based: P=89/130=0.6846 R=89/160=0.5563 F=0.6138
* LVC.full: Tok-based: P=183/251=0.7291 R=183/333=0.5495 F=0.6267
* MVC: MWE-proportion: gold=4/498=1% pred=0/429=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* VID: MWE-proportion: gold=212/498=43% pred=191/429=45%
* VID: MWE-based: P=65/191=0.3403 R=65/212=0.3066 F=0.3226
* VID: Tok-based: P=287/372=0.7715 R=287/586=0.4898 F=0.5992

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=281/498=56% pred=196/429=46%
* Continuous: MWE-based: P=150/196=0.7653 R=150/281=0.5338 F=0.6289
* Discontinuous: MWE-proportion: gold=217/498=44% pred=233/429=54%
* Discontinuous: MWE-based: P=98/233=0.4206 R=98/217=0.4516 F=0.4356

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=409/429=95%
* Multi-token: MWE-based: P=248/409=0.6064 R=248/498=0.4980 F=0.5469

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=251/498=50% pred=158/429=37%
* Seen-in-train: MWE-based: P=149/158=0.9430 R=149/251=0.5936 F=0.7286
* Unseen-in-train: MWE-proportion: gold=247/498=50% pred=271/429=63%
* Unseen-in-train: MWE-based: P=99/271=0.3653 R=99/247=0.4008 F=0.3822

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=126/251=50% pred=78/158=49%
* Variant-of-train: MWE-based: P=74/78=0.9487 R=74/126=0.5873 F=0.7255
* Identical-to-train: MWE-proportion: gold=125/251=50% pred=80/158=51%
* Identical-to-train: MWE-based: P=75/80=0.9375 R=75/125=0.6000 F=0.7317


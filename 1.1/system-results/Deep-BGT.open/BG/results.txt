## Global evaluation
* MWE-based: P=355/413=0.8596 R=355/670=0.5299 F=0.6556
* Tok-based: P=748/822=0.9100 R=748/1416=0.5282 F=0.6685

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=8/670=1% pred=0/413=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/20=0.0000 F=0.0000
* IRV: MWE-proportion: gold=254/670=38% pred=243/413=59%
* IRV: MWE-based: P=217/243=0.8930 R=217/254=0.8543 F=0.8732
* IRV: Tok-based: P=435/486=0.8951 R=435/508=0.8563 F=0.8753
* LVC.cause: MWE-proportion: gold=52/670=8% pred=23/413=6%
* LVC.cause: MWE-based: P=10/23=0.4348 R=10/52=0.1923 F=0.2667
* LVC.cause: Tok-based: P=21/46=0.4565 R=21/114=0.1842 F=0.2625
* LVC.full: MWE-proportion: gold=274/670=41% pred=113/413=27%
* LVC.full: MWE-based: P=98/113=0.8673 R=98/274=0.3577 F=0.5065
* LVC.full: Tok-based: P=204/224=0.9107 R=204/569=0.3585 F=0.5145
* VID: MWE-proportion: gold=82/670=12% pred=34/413=8%
* VID: MWE-based: P=14/34=0.4118 R=14/82=0.1707 F=0.2414
* VID: Tok-based: P=43/66=0.6515 R=43/205=0.2098 F=0.3173

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=476/670=71% pred=352/413=85%
* Continuous: MWE-based: P=310/352=0.8807 R=310/476=0.6513 F=0.7488
* Discontinuous: MWE-proportion: gold=194/670=29% pred=61/413=15%
* Discontinuous: MWE-based: P=45/61=0.7377 R=45/194=0.2320 F=0.3529

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=670/670=100% pred=409/413=99%
* Multi-token: MWE-based: P=355/409=0.8680 R=355/670=0.5299 F=0.6580

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=450/670=67% pred=330/413=80%
* Seen-in-train: MWE-based: P=308/330=0.9333 R=308/450=0.6844 F=0.7897
* Unseen-in-train: MWE-proportion: gold=220/670=33% pred=83/413=20%
* Unseen-in-train: MWE-based: P=47/83=0.5663 R=47/220=0.2136 F=0.3102

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=160/450=36% pred=70/330=21%
* Variant-of-train: MWE-based: P=66/70=0.9429 R=66/160=0.4125 F=0.5739
* Identical-to-train: MWE-proportion: gold=290/450=64% pred=260/330=79%
* Identical-to-train: MWE-based: P=242/260=0.9308 R=242/290=0.8345 F=0.8800


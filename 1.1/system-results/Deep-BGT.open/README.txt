Deep-BGT
Berna Erden, Gözde Berk and Tunga Güngör
(Submission #22)

Summary
We built Bidirectional LSTM-CRF model for MWE tagging. Also, we used gappy 1-level tagging scheme which is a variant of IOB tagging.

FORM, POS and DEPREL information given in cupt files are used. We used pre-trained word embeddings from Fast-text as additional resource. Word embeddings are available at https://github.com/facebookresearch/fastText/blob/master/docs/crawl-vectors.md

PS: We participated in the creation of Turkish dataset. PS: We used the dev data as additional training data.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	OPEN
 	
Languages:  	
Bulgarian (BG)
German (DE)
French (FR)
Hungarian (HU)
Italian (IT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)

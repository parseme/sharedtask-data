## Global evaluation
* MWE-based: P=293/397=0.7380 R=293/501=0.5848 F=0.6526
* Tok-based: P=726/805=0.9019 R=726/1113=0.6523 F=0.7570

## Per-category evaluation (partition of Global)
* LVC.full: MWE-proportion: gold=501/501=100% pred=397/397=100%
* LVC.full: MWE-based: P=293/397=0.7380 R=293/501=0.5848 F=0.6526
* LVC.full: Tok-based: P=726/805=0.9019 R=726/1113=0.6523 F=0.7570

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=398/501=79% pred=349/397=88%
* Continuous: MWE-based: P=264/349=0.7564 R=264/398=0.6633 F=0.7068
* Discontinuous: MWE-proportion: gold=103/501=21% pred=48/397=12%
* Discontinuous: MWE-based: P=29/48=0.6042 R=29/103=0.2816 F=0.3841

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=501/501=100% pred=342/397=86%
* Multi-token: MWE-based: P=293/342=0.8567 R=293/501=0.5848 F=0.6951

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=313/501=62% pred=229/397=58%
* Seen-in-train: MWE-based: P=220/229=0.9607 R=220/313=0.7029 F=0.8118
* Unseen-in-train: MWE-proportion: gold=188/501=38% pred=168/397=42%
* Unseen-in-train: MWE-based: P=73/168=0.4345 R=73/188=0.3883 F=0.4101

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=167/313=53% pred=107/229=47%
* Variant-of-train: MWE-based: P=102/107=0.9533 R=102/167=0.6108 F=0.7445
* Identical-to-train: MWE-proportion: gold=146/313=47% pred=122/229=53%
* Identical-to-train: MWE-based: P=118/122=0.9672 R=118/146=0.8082 F=0.8806


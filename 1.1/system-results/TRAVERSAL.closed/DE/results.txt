## Global evaluation
* MWE-based: P=163/259=0.6293 R=163/498=0.3273 F=0.4306
* Tok-based: P=374/491=0.7617 R=374/975=0.3836 F=0.5102

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=40/498=8% pred=30/259=12%
* IRV: MWE-based: P=12/30=0.4000 R=12/40=0.3000 F=0.3429
* IRV: Tok-based: P=43/72=0.5972 R=43/96=0.4479 F=0.5119
* LVC.cause: MWE-proportion: gold=2/498=0% pred=1/259=0%
* LVC.cause: MWE-based: P=0/1=0.0000 R=0/2=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/1=0.0000 R=0/5=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=42/498=8% pred=10/259=4%
* LVC.full: MWE-based: P=4/10=0.4000 R=4/42=0.0952 F=0.1538
* LVC.full: Tok-based: P=13/21=0.6190 R=13/97=0.1340 F=0.2203
* VID: MWE-proportion: gold=183/498=37% pred=89/259=34%
* VID: MWE-based: P=37/89=0.4157 R=37/183=0.2022 F=0.2721
* VID: Tok-based: P=131/202=0.6485 R=131/462=0.2835 F=0.3946
* VPC.full: MWE-proportion: gold=210/498=42% pred=122/259=47%
* VPC.full: MWE-based: P=100/122=0.8197 R=100/210=0.4762 F=0.6024
* VPC.full: Tok-based: P=158/186=0.8495 R=158/291=0.5430 F=0.6625
* VPC.semi: MWE-proportion: gold=23/498=5% pred=7/259=3%
* VPC.semi: MWE-based: P=4/7=0.5714 R=4/23=0.1739 F=0.2667
* VPC.semi: Tok-based: P=6/9=0.6667 R=6/31=0.1935 F=0.3000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=267/498=54% pred=148/259=57%
* Continuous: MWE-based: P=89/148=0.6014 R=89/267=0.3333 F=0.4289
* Discontinuous: MWE-proportion: gold=231/498=46% pred=111/259=43%
* Discontinuous: MWE-based: P=74/111=0.6667 R=74/231=0.3203 F=0.4327

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=349/498=70% pred=172/259=66%
* Multi-token: MWE-based: P=112/172=0.6512 R=112/349=0.3209 F=0.4299
* Single-token: MWE-proportion: gold=149/498=30% pred=87/259=34%
* Single-token: MWE-based: P=51/87=0.5862 R=51/149=0.3423 F=0.4322

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=250/498=50% pred=166/259=64%
* Seen-in-train: MWE-based: P=142/166=0.8554 R=142/250=0.5680 F=0.6827
* Unseen-in-train: MWE-proportion: gold=248/498=50% pred=93/259=36%
* Unseen-in-train: MWE-based: P=21/93=0.2258 R=21/248=0.0847 F=0.1232

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=148/250=59% pred=85/166=51%
* Variant-of-train: MWE-based: P=73/85=0.8588 R=73/148=0.4932 F=0.6266
* Identical-to-train: MWE-proportion: gold=102/250=41% pred=81/166=49%
* Identical-to-train: MWE-based: P=69/81=0.8519 R=69/102=0.6765 F=0.7541


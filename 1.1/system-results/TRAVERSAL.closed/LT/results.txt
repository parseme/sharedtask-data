## Global evaluation
* MWE-based: P=69/233=0.2961 R=69/500=0.1380 F=0.1883
* Tok-based: P=190/342=0.5556 R=190/1123=0.1692 F=0.2594

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=14/500=3% pred=0/233=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/14=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=284/500=57% pred=199/233=85%
* LVC.full: MWE-based: P=55/199=0.2764 R=55/284=0.1937 F=0.2277
* LVC.full: Tok-based: P=160/281=0.5694 R=160/569=0.2812 F=0.3765
* VID: MWE-proportion: gold=202/500=40% pred=34/233=15%
* VID: MWE-based: P=14/34=0.4118 R=14/202=0.0693 F=0.1186
* VID: Tok-based: P=29/61=0.4754 R=29/526=0.0551 F=0.0988

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=302/500=60% pred=233/233=100%
* Continuous: MWE-based: P=69/233=0.2961 R=69/302=0.2285 F=0.2579
* Discontinuous: MWE-proportion: gold=198/500=40% pred=0/233=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/198=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=100/233=43%
* Multi-token: MWE-based: P=69/100=0.6900 R=69/500=0.1380 F=0.2300

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=225/500=45% pred=77/233=33%
* Seen-in-train: MWE-based: P=64/77=0.8312 R=64/225=0.2844 F=0.4238
* Unseen-in-train: MWE-proportion: gold=275/500=55% pred=156/233=67%
* Unseen-in-train: MWE-based: P=5/156=0.0321 R=5/275=0.0182 F=0.0232

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=186/225=83% pred=51/77=66%
* Variant-of-train: MWE-based: P=39/51=0.7647 R=39/186=0.2097 F=0.3291
* Identical-to-train: MWE-proportion: gold=39/225=17% pred=26/77=34%
* Identical-to-train: MWE-based: P=25/26=0.9615 R=25/39=0.6410 F=0.7692


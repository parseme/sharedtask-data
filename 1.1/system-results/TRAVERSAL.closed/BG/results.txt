## Global evaluation
* MWE-based: P=319/422=0.7559 R=319/670=0.4761 F=0.5842
* Tok-based: P=705/856=0.8236 R=705/1416=0.4979 F=0.6206

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=8/670=1% pred=3/422=1%
* IAV: MWE-based: P=0/3=0.0000 R=0/8=0.0000 F=0.0000
* IAV: Tok-based: P=0/6=0.0000 R=0/20=0.0000 F=0.0000
* IRV: MWE-proportion: gold=254/670=38% pred=246/422=58%
* IRV: MWE-based: P=200/246=0.8130 R=200/254=0.7874 F=0.8000
* IRV: Tok-based: P=419/509=0.8232 R=419/508=0.8248 F=0.8240
* LVC.cause: MWE-proportion: gold=52/670=8% pred=6/422=1%
* LVC.cause: MWE-based: P=5/6=0.8333 R=5/52=0.0962 F=0.1724
* LVC.cause: Tok-based: P=13/15=0.8667 R=13/114=0.1140 F=0.2016
* LVC.full: MWE-proportion: gold=274/670=41% pred=118/422=28%
* LVC.full: MWE-based: P=86/118=0.7288 R=86/274=0.3139 F=0.4388
* LVC.full: Tok-based: P=189/222=0.8514 R=189/569=0.3322 F=0.4779
* VID: MWE-proportion: gold=82/670=12% pred=49/422=12%
* VID: MWE-based: P=23/49=0.4694 R=23/82=0.2805 F=0.3511
* VID: Tok-based: P=62/104=0.5962 R=62/205=0.3024 F=0.4013

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=476/670=71% pred=364/422=86%
* Continuous: MWE-based: P=276/364=0.7582 R=276/476=0.5798 F=0.6571
* Discontinuous: MWE-proportion: gold=194/670=29% pred=58/422=14%
* Discontinuous: MWE-based: P=43/58=0.7414 R=43/194=0.2216 F=0.3413

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=670/670=100% pred=394/422=93%
* Multi-token: MWE-based: P=319/394=0.8096 R=319/670=0.4761 F=0.5996

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=450/670=67% pred=339/422=80%
* Seen-in-train: MWE-based: P=300/339=0.8850 R=300/450=0.6667 F=0.7605
* Unseen-in-train: MWE-proportion: gold=220/670=33% pred=83/422=20%
* Unseen-in-train: MWE-based: P=19/83=0.2289 R=19/220=0.0864 F=0.1254

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=160/450=36% pred=74/339=22%
* Variant-of-train: MWE-based: P=64/74=0.8649 R=64/160=0.4000 F=0.5470
* Identical-to-train: MWE-proportion: gold=290/450=64% pred=265/339=78%
* Identical-to-train: MWE-based: P=236/265=0.8906 R=236/290=0.8138 F=0.8505


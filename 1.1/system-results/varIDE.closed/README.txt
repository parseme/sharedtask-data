varIDE
Caroline Pasquer, Agata Savary, Carlos Ramisch and Jean-Yves Antoine
(Submission #6)

Summary
Our system relies on a Naive Bayes Classifier using absolute and comparative features with training data. We first represent the profile of any verbal MWE by its morphological, linear (insertions between lexicalized components) and syntactic features (outgoing dependencies). For each language, we extract in Train the ten most frequent POS pattern variations, e.g. in English VERB-DET-NOUN may alternate with DET-NOUN-VERB but not NOUN-VERB-DET. Then, we search candidates in the training and test data that share both same combinations of lemmas as manually annotated and one of the selected POS patterns. This system is thus designed to focus on variant identification. The extracted candidates in Train are labelled as 'idiomatic' or 'literal' depending on their manual annotation or not. Finally, a Naive Bayes classifier uses the features of the candidates in training and test corpora to determine which in Test can be idiomatic.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)

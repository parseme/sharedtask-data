## Global evaluation
* MWE-based: P=34/43=0.7907 R=34/498=0.0683 F=0.1257
* Tok-based: P=77/92=0.8370 R=77/1092=0.0705 F=0.1301

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=188/498=38% pred=4/43=9%
* IAV: MWE-based: P=4/4=1.0000 R=4/188=0.0213 F=0.0417
* IAV: Tok-based: P=12/12=1.0000 R=12/420=0.0286 F=0.0556
* IRV: MWE-proportion: gold=118/498=24% pred=0/43=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/118=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/237=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=31/498=6% pred=6/43=14%
* LVC.cause: MWE-based: P=5/6=0.8333 R=5/31=0.1613 F=0.2703
* LVC.cause: Tok-based: P=10/12=0.8333 R=10/64=0.1562 F=0.2632
* LVC.full: MWE-proportion: gold=131/498=26% pred=31/43=72%
* LVC.full: MWE-based: P=23/31=0.7419 R=23/131=0.1756 F=0.2840
* LVC.full: Tok-based: P=51/64=0.7969 R=51/293=0.1741 F=0.2857
* VID: MWE-proportion: gold=33/498=7% pred=2/43=5%
* VID: MWE-based: P=2/2=1.0000 R=2/33=0.0606 F=0.1143
* VID: Tok-based: P=4/4=1.0000 R=4/86=0.0465 F=0.0889

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=291/498=58% pred=15/43=35%
* Continuous: MWE-based: P=15/15=1.0000 R=15/291=0.0515 F=0.0980
* Discontinuous: MWE-proportion: gold=207/498=42% pred=28/43=65%
* Discontinuous: MWE-based: P=19/28=0.6786 R=19/207=0.0918 F=0.1617

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=43/43=100%
* Multi-token: MWE-based: P=34/43=0.7907 R=34/498=0.0683 F=0.1257

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=273/498=55% pred=43/43=100%
* Seen-in-train: MWE-based: P=34/43=0.7907 R=34/273=0.1245 F=0.2152
* Unseen-in-train: MWE-proportion: gold=225/498=45% pred=0/43=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/225=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=199/273=73% pred=38/43=88%
* Variant-of-train: MWE-based: P=29/38=0.7632 R=29/199=0.1457 F=0.2447
* Identical-to-train: MWE-proportion: gold=74/273=27% pred=5/43=12%
* Identical-to-train: MWE-based: P=5/5=1.0000 R=5/74=0.0676 F=0.1266


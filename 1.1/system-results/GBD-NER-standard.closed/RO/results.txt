## Global evaluation
* MWE-based: P=504/1283=0.3928 R=504/589=0.8557 F=0.5385
* Tok-based: P=1103/2676=0.4122 R=1103/1268=0.8699 F=0.5593

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=363/589=62% pred=715/1283=56%
* IRV: MWE-based: P=334/715=0.4671 R=334/363=0.9201 F=0.6197
* IRV: Tok-based: P=671/1430=0.4692 R=671/726=0.9242 F=0.6224
* LVC.cause: MWE-proportion: gold=19/589=3% pred=41/1283=3%
* LVC.cause: MWE-based: P=17/41=0.4146 R=17/19=0.8947 F=0.5667
* LVC.cause: Tok-based: P=52/114=0.4561 R=52/56=0.9286 F=0.6118
* LVC.full: MWE-proportion: gold=34/589=6% pred=129/1283=10%
* LVC.full: MWE-based: P=17/129=0.1318 R=17/34=0.5000 F=0.2086
* LVC.full: Tok-based: P=34/258=0.1318 R=34/71=0.4789 F=0.2067
* VID: MWE-proportion: gold=173/589=29% pred=398/1283=31%
* VID: MWE-based: P=124/398=0.3116 R=124/173=0.7168 F=0.4343
* VID: Tok-based: P=292/874=0.3341 R=292/415=0.7036 F=0.4531

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=393/589=67% pred=908/1283=71%
* Continuous: MWE-based: P=348/908=0.3833 R=348/393=0.8855 F=0.5350
* Discontinuous: MWE-proportion: gold=196/589=33% pred=375/1283=29%
* Discontinuous: MWE-based: P=156/375=0.4160 R=156/196=0.7959 F=0.5464

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=589/589=100% pred=1283/1283=100%
* Multi-token: MWE-based: P=504/1283=0.3928 R=504/589=0.8557 F=0.5385

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=561/589=95% pred=624/1283=49%
* Seen-in-train: MWE-based: P=489/624=0.7837 R=489/561=0.8717 F=0.8253
* Unseen-in-train: MWE-proportion: gold=28/589=5% pred=659/1283=51%
* Unseen-in-train: MWE-based: P=15/659=0.0228 R=15/28=0.5357 F=0.0437

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=65/561=12% pred=130/624=21%
* Variant-of-train: MWE-based: P=38/130=0.2923 R=38/65=0.5846 F=0.3897
* Identical-to-train: MWE-proportion: gold=496/561=88% pred=494/624=79%
* Identical-to-train: MWE-based: P=451/494=0.9130 R=451/496=0.9093 F=0.9111


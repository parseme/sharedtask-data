## Global evaluation
* MWE-based: P=217/594=0.3653 R=217/498=0.4357 F=0.3974
* Tok-based: P=530/1204=0.4402 R=530/1092=0.4853 F=0.4617

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=188/498=38% pred=225/594=38%
* IAV: MWE-based: P=87/225=0.3867 R=87/188=0.4628 F=0.4213
* IAV: Tok-based: P=230/462=0.4978 R=230/420=0.5476 F=0.5215
* IRV: MWE-proportion: gold=118/498=24% pred=142/594=24%
* IRV: MWE-based: P=72/142=0.5070 R=72/118=0.6102 F=0.5538
* IRV: Tok-based: P=146/284=0.5141 R=146/237=0.6160 F=0.5605
* LVC.cause: MWE-proportion: gold=31/498=6% pred=0/594=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/31=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/64=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=131/498=26% pred=225/594=38%
* LVC.full: MWE-based: P=42/225=0.1867 R=42/131=0.3206 F=0.2360
* LVC.full: Tok-based: P=87/452=0.1925 R=87/293=0.2969 F=0.2336
* VID: MWE-proportion: gold=33/498=7% pred=2/594=0%
* VID: MWE-based: P=0/2=0.0000 R=0/33=0.0000 F=0.0000
* VID: Tok-based: P=0/6=0.0000 R=0/86=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=291/498=58% pred=360/594=61%
* Continuous: MWE-based: P=160/360=0.4444 R=160/291=0.5498 F=0.4916
* Discontinuous: MWE-proportion: gold=207/498=42% pred=234/594=39%
* Discontinuous: MWE-based: P=57/234=0.2436 R=57/207=0.2754 F=0.2585

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=594/594=100%
* Multi-token: MWE-based: P=217/594=0.3653 R=217/498=0.4357 F=0.3974

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=273/498=55% pred=144/594=24%
* Seen-in-train: MWE-based: P=137/144=0.9514 R=137/273=0.5018 F=0.6571
* Unseen-in-train: MWE-proportion: gold=225/498=45% pred=450/594=76%
* Unseen-in-train: MWE-based: P=80/450=0.1778 R=80/225=0.3556 F=0.2370

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=199/273=73% pred=100/144=69%
* Variant-of-train: MWE-based: P=93/100=0.9300 R=93/199=0.4673 F=0.6221
* Identical-to-train: MWE-proportion: gold=74/273=27% pred=44/144=31%
* Identical-to-train: MWE-based: P=44/44=1.0000 R=44/74=0.5946 F=0.7458


## Global evaluation
* MWE-based: P=148/958=0.1545 R=148/496=0.2984 F=0.2036
* Tok-based: P=458/2019=0.2268 R=458/1292=0.3545 F=0.2767

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=41/496=8% pred=82/958=9%
* IAV: MWE-based: P=4/82=0.0488 R=4/41=0.0976 F=0.0650
* IAV: Tok-based: P=12/165=0.0727 R=12/101=0.1188 F=0.0902
* IRV: MWE-proportion: gold=96/496=19% pred=345/958=36%
* IRV: MWE-based: P=72/345=0.2087 R=72/96=0.7500 F=0.3265
* IRV: Tok-based: P=145/690=0.2101 R=145/192=0.7552 F=0.3288
* LS.ICV: MWE-proportion: gold=8/496=2% pred=0/958=0%
* LS.ICV: MWE-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* LS.ICV: Tok-based: P=0/0=0.0000 R=0/17=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=25/496=5% pred=0/958=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/25=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/53=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=104/496=21% pred=344/958=36%
* LVC.full: MWE-based: P=21/344=0.0610 R=21/104=0.2019 F=0.0938
* LVC.full: Tok-based: P=47/688=0.0683 R=47/231=0.2035 F=0.1023
* MVC: MWE-proportion: gold=5/496=1% pred=3/958=0%
* MVC: MWE-based: P=0/3=0.0000 R=0/5=0.0000 F=0.0000
* MVC: Tok-based: P=0/7=0.0000 R=0/11=0.0000 F=0.0000
* VID: MWE-proportion: gold=201/496=41% pred=177/958=18%
* VID: MWE-based: P=18/177=0.1017 R=18/201=0.0896 F=0.0952
* VID: Tok-based: P=62/455=0.1363 R=62/652=0.0951 F=0.1120
* VPC.full: MWE-proportion: gold=23/496=5% pred=7/958=1%
* VPC.full: MWE-based: P=0/7=0.0000 R=0/23=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/14=0.0000 R=0/50=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=331/496=67% pred=620/958=65%
* Continuous: MWE-based: P=120/620=0.1935 R=120/331=0.3625 F=0.2524
* Discontinuous: MWE-proportion: gold=165/496=33% pred=338/958=35%
* Discontinuous: MWE-based: P=28/338=0.0828 R=28/165=0.1697 F=0.1113

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=496/496=100% pred=958/958=100%
* Multi-token: MWE-based: P=148/958=0.1545 R=148/496=0.2984 F=0.2036

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=297/496=60% pred=166/958=17%
* Seen-in-train: MWE-based: P=112/166=0.6747 R=112/297=0.3771 F=0.4838
* Unseen-in-train: MWE-proportion: gold=199/496=40% pred=792/958=83%
* Unseen-in-train: MWE-based: P=36/792=0.0455 R=36/199=0.1809 F=0.0727

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=184/297=62% pred=88/166=53%
* Variant-of-train: MWE-based: P=60/88=0.6818 R=60/184=0.3261 F=0.4412
* Identical-to-train: MWE-proportion: gold=113/297=38% pred=78/166=47%
* Identical-to-train: MWE-based: P=52/78=0.6667 R=52/113=0.4602 F=0.5445


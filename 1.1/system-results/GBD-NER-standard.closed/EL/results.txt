## Global evaluation
* MWE-based: P=198/475=0.4168 R=198/501=0.3952 F=0.4057
* Tok-based: P=532/1024=0.5195 R=532/1195=0.4452 F=0.4795

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=11/501=2% pred=0/475=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/11=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/25=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=308/501=61% pred=412/475=87%
* LVC.full: MWE-based: P=136/412=0.3301 R=136/308=0.4416 F=0.3778
* LVC.full: Tok-based: P=295/824=0.3580 R=295/641=0.4602 F=0.4027
* MVC: MWE-proportion: gold=2/501=0% pred=0/475=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/6=0.0000 F=0.0000
* VID: MWE-proportion: gold=169/501=34% pred=63/475=13%
* VID: MWE-based: P=12/63=0.1905 R=12/169=0.0710 F=0.1034
* VID: Tok-based: P=76/200=0.3800 R=76/500=0.1520 F=0.2171
* VPC.full: MWE-proportion: gold=11/501=2% pred=0/475=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/11=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/23=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=275/501=55% pred=311/475=65%
* Continuous: MWE-based: P=135/311=0.4341 R=135/275=0.4909 F=0.4608
* Discontinuous: MWE-proportion: gold=226/501=45% pred=164/475=35%
* Discontinuous: MWE-based: P=63/164=0.3841 R=63/226=0.2788 F=0.3231

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=501/501=100% pred=475/475=100%
* Multi-token: MWE-based: P=198/475=0.4168 R=198/501=0.3952 F=0.4057

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=289/501=58% pred=176/475=37%
* Seen-in-train: MWE-based: P=157/176=0.8920 R=157/289=0.5433 F=0.6753
* Unseen-in-train: MWE-proportion: gold=212/501=42% pred=299/475=63%
* Unseen-in-train: MWE-based: P=41/299=0.1371 R=41/212=0.1934 F=0.1605

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=197/289=68% pred=106/176=60%
* Variant-of-train: MWE-based: P=90/106=0.8491 R=90/197=0.4569 F=0.5941
* Identical-to-train: MWE-proportion: gold=92/289=32% pred=70/176=40%
* Identical-to-train: MWE-based: P=67/70=0.9571 R=67/92=0.7283 F=0.8272


## Global evaluation
* MWE-based: P=139/526=0.2643 R=139/498=0.2791 F=0.2715
* Tok-based: P=396/1101=0.3597 R=396/975=0.4062 F=0.3815

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=40/498=8% pred=56/526=11%
* IRV: MWE-based: P=14/56=0.2500 R=14/40=0.3500 F=0.2917
* IRV: Tok-based: P=44/120=0.3667 R=44/96=0.4583 F=0.4074
* LVC.cause: MWE-proportion: gold=2/498=0% pred=0/526=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/5=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=42/498=8% pred=0/526=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/42=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/97=0.0000 F=0.0000
* VID: MWE-proportion: gold=183/498=37% pred=313/526=60%
* VID: MWE-based: P=43/313=0.1374 R=43/183=0.2350 F=0.1734
* VID: Tok-based: P=145/667=0.2174 R=145/462=0.3139 F=0.2569
* VPC.full: MWE-proportion: gold=210/498=42% pred=157/526=30%
* VPC.full: MWE-based: P=63/157=0.4013 R=63/210=0.3000 F=0.3433
* VPC.full: Tok-based: P=126/314=0.4013 R=126/291=0.4330 F=0.4165
* VPC.semi: MWE-proportion: gold=23/498=5% pred=0/526=0%
* VPC.semi: MWE-based: P=0/0=0.0000 R=0/23=0.0000 F=0.0000
* VPC.semi: Tok-based: P=0/0=0.0000 R=0/31=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=267/498=54% pred=239/526=45%
* Continuous: MWE-based: P=54/239=0.2259 R=54/267=0.2022 F=0.2134
* Discontinuous: MWE-proportion: gold=231/498=46% pred=287/526=55%
* Discontinuous: MWE-based: P=85/287=0.2962 R=85/231=0.3680 F=0.3282

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=349/498=70% pred=526/526=100%
* Multi-token: MWE-based: P=139/526=0.2643 R=139/349=0.3983 F=0.3177
* Single-token: MWE-proportion: gold=149/498=30% pred=0/526=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/149=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=250/498=50% pred=119/526=23%
* Seen-in-train: MWE-based: P=91/119=0.7647 R=91/250=0.3640 F=0.4932
* Unseen-in-train: MWE-proportion: gold=248/498=50% pred=407/526=77%
* Unseen-in-train: MWE-based: P=48/407=0.1179 R=48/248=0.1935 F=0.1466

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=148/250=59% pred=71/119=60%
* Variant-of-train: MWE-based: P=58/71=0.8169 R=58/148=0.3919 F=0.5297
* Identical-to-train: MWE-proportion: gold=102/250=41% pred=48/119=40%
* Identical-to-train: MWE-based: P=33/48=0.6875 R=33/102=0.3235 F=0.4400


## Global evaluation
* MWE-based: P=463/917=0.5049 R=463/670=0.6910 F=0.5835
* Tok-based: P=998/1885=0.5294 R=998/1416=0.7048 F=0.6047

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=8/670=1% pred=41/917=4%
* IAV: MWE-based: P=0/41=0.0000 R=0/8=0.0000 F=0.0000
* IAV: Tok-based: P=0/82=0.0000 R=0/20=0.0000 F=0.0000
* IRV: MWE-proportion: gold=254/670=38% pred=369/917=40%
* IRV: MWE-based: P=241/369=0.6531 R=241/254=0.9488 F=0.7737
* IRV: Tok-based: P=484/738=0.6558 R=484/508=0.9528 F=0.7769
* LVC.cause: MWE-proportion: gold=52/670=8% pred=0/917=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/52=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/114=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=274/670=41% pred=347/917=38%
* LVC.full: MWE-based: P=154/347=0.4438 R=154/274=0.5620 F=0.4960
* LVC.full: Tok-based: P=316/705=0.4482 R=316/569=0.5554 F=0.4961
* VID: MWE-proportion: gold=82/670=12% pred=160/917=17%
* VID: MWE-based: P=32/160=0.2000 R=32/82=0.3902 F=0.2645
* VID: Tok-based: P=86/360=0.2389 R=86/205=0.4195 F=0.3044

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=476/670=71% pred=745/917=81%
* Continuous: MWE-based: P=381/745=0.5114 R=381/476=0.8004 F=0.6241
* Discontinuous: MWE-proportion: gold=194/670=29% pred=172/917=19%
* Discontinuous: MWE-based: P=82/172=0.4767 R=82/194=0.4227 F=0.4481

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=670/670=100% pred=917/917=100%
* Multi-token: MWE-based: P=463/917=0.5049 R=463/670=0.6910 F=0.5835

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=450/670=67% pred=483/917=53%
* Seen-in-train: MWE-based: P=381/483=0.7888 R=381/450=0.8467 F=0.8167
* Unseen-in-train: MWE-proportion: gold=220/670=33% pred=434/917=47%
* Unseen-in-train: MWE-based: P=82/434=0.1889 R=82/220=0.3727 F=0.2508

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=160/450=36% pred=137/483=28%
* Variant-of-train: MWE-based: P=103/137=0.7518 R=103/160=0.6438 F=0.6936
* Identical-to-train: MWE-proportion: gold=290/450=64% pred=346/483=72%
* Identical-to-train: MWE-based: P=278/346=0.8035 R=278/290=0.9586 F=0.8742


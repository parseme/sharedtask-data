## Global evaluation
* MWE-based: P=20/24=0.8333 R=20/496=0.0403 F=0.0769
* Tok-based: P=45/55=0.8182 R=45/1292=0.0348 F=0.0668

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=41/496=8% pred=0/24=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/41=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/101=0.0000 F=0.0000
* IRV: MWE-proportion: gold=96/496=19% pred=0/24=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/96=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/192=0.0000 F=0.0000
* LS.ICV: MWE-proportion: gold=8/496=2% pred=0/24=0%
* LS.ICV: MWE-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* LS.ICV: Tok-based: P=0/0=0.0000 R=0/17=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=25/496=5% pred=0/24=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/25=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/53=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=104/496=21% pred=0/24=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/104=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/231=0.0000 F=0.0000
* MVC: MWE-proportion: gold=5/496=1% pred=0/24=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/5=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/11=0.0000 F=0.0000
* VID: MWE-proportion: gold=201/496=41% pred=24/24=100%
* VID: MWE-based: P=7/24=0.2917 R=7/201=0.0348 F=0.0622
* VID: Tok-based: P=19/55=0.3455 R=19/652=0.0291 F=0.0537
* VPC.full: MWE-proportion: gold=23/496=5% pred=0/24=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/23=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/50=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=331/496=67% pred=24/24=100%
* Continuous: MWE-based: P=20/24=0.8333 R=20/331=0.0604 F=0.1127
* Discontinuous: MWE-proportion: gold=165/496=33% pred=0/24=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/165=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=496/496=100% pred=24/24=100%
* Multi-token: MWE-based: P=20/24=0.8333 R=20/496=0.0403 F=0.0769

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=297/496=60% pred=20/24=83%
* Seen-in-train: MWE-based: P=20/20=1.0000 R=20/297=0.0673 F=0.1262
* Unseen-in-train: MWE-proportion: gold=199/496=40% pred=4/24=17%
* Unseen-in-train: MWE-based: P=0/4=0.0000 R=0/199=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=184/297=62% pred=5/20=25%
* Variant-of-train: MWE-based: P=5/5=1.0000 R=5/184=0.0272 F=0.0529
* Identical-to-train: MWE-proportion: gold=113/297=38% pred=15/20=75%
* Identical-to-train: MWE-based: P=15/15=1.0000 R=15/113=0.1327 F=0.2344


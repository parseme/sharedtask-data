Polirem
Valeria Quochi and Roberto Bartolini
(Submission #14)

Summary
The system is based on a computationally "light" tool/web service that produces multi word candidates for creating/enriching lexical resources, potentially for any language. (https://ilc4clarin.ilc.cnr.it/services/soaplab2-axis/ > extractor_mw (WSDL)

The method applied is inspired by the seminal work by Smadja (1993), and integrates consolidated statistical methods and association measures for further filtering of the acquired candidates. A version of the tool is available, under the MIT license, at: https://github.com/francescafrontini/MWExtractor

MWE candidates in this experiment are acquired from the train and dev data for this 1.1 edition of the VMWE shared task and then "annotated" on the test data in a second step on the basis of basic heuristics. The system targets mainly VIDs and LVCs, but does not classify them. The VID label is used in all cases, as it is the most frequent. Thus, per category evaluation is will not be meaningful.

The extraction plus annotation system has been tuned for Italian and then tested also on PT and FR. Details will be given in the system description paper.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
French (FR)
Italian (IT)
Brazilian Portuguese (PT)

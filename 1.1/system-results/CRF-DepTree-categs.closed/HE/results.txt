## Global evaluation
* MWE-based: P=48/214=0.2243 R=48/502=0.0956 F=0.1341
* Tok-based: P=163/431=0.3782 R=163/1155=0.1411 F=0.2055

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=49/502=10% pred=19/214=9%
* LVC.cause: MWE-based: P=5/19=0.2632 R=5/49=0.1020 F=0.1471
* LVC.cause: Tok-based: P=14/27=0.5185 R=14/101=0.1386 F=0.2188
* LVC.full: MWE-proportion: gold=211/502=42% pred=82/214=38%
* LVC.full: MWE-based: P=32/82=0.3902 R=32/211=0.1517 F=0.2184
* LVC.full: Tok-based: P=86/156=0.5513 R=86/435=0.1977 F=0.2910
* VID: MWE-proportion: gold=182/502=36% pred=90/214=42%
* VID: MWE-based: P=9/90=0.1000 R=9/182=0.0495 F=0.0662
* VID: Tok-based: P=45/223=0.2018 R=45/467=0.0964 F=0.1304
* VPC.full: MWE-proportion: gold=60/502=12% pred=25/214=12%
* VPC.full: MWE-based: P=0/25=0.0000 R=0/60=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/28=0.0000 R=0/152=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=384/502=76% pred=178/214=83%
* Continuous: MWE-based: P=43/178=0.2416 R=43/384=0.1120 F=0.1530
* Discontinuous: MWE-proportion: gold=118/502=24% pred=36/214=17%
* Discontinuous: MWE-based: P=5/36=0.1389 R=5/118=0.0424 F=0.0649

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/502=100% pred=128/214=60%
* Multi-token: MWE-based: P=48/128=0.3750 R=48/502=0.0956 F=0.1524

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=174/502=35% pred=50/214=23%
* Seen-in-train: MWE-based: P=43/50=0.8600 R=43/174=0.2471 F=0.3839
* Unseen-in-train: MWE-proportion: gold=328/502=65% pred=164/214=77%
* Unseen-in-train: MWE-based: P=5/164=0.0305 R=5/328=0.0152 F=0.0203

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=72/174=41% pred=23/50=46%
* Variant-of-train: MWE-based: P=16/23=0.6957 R=16/72=0.2222 F=0.3368
* Identical-to-train: MWE-proportion: gold=102/174=59% pred=27/50=54%
* Identical-to-train: MWE-based: P=27/27=1.0000 R=27/102=0.2647 F=0.4186


## Global evaluation
* MWE-based: P=151/277=0.5451 R=151/500=0.3020 F=0.3887
* Tok-based: P=416/494=0.8421 R=416/1113=0.3738 F=0.5177

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=101/500=20% pred=277/277=100%
* IAV: MWE-based: P=37/277=0.1336 R=37/101=0.3663 F=0.1958
* IAV: Tok-based: P=96/494=0.1943 R=96/221=0.4344 F=0.2685
* IRV: MWE-proportion: gold=245/500=49% pred=0/277=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/245=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/490=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=13/500=3% pred=0/277=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/13=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/31=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=35/500=7% pred=0/277=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/35=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/75=0.0000 F=0.0000
* VID: MWE-proportion: gold=106/500=21% pred=0/277=0%
* VID: MWE-based: P=0/0=0.0000 R=0/106=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/296=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=244/500=49% pred=215/277=78%
* Continuous: MWE-based: P=97/215=0.4512 R=97/244=0.3975 F=0.4227
* Discontinuous: MWE-proportion: gold=256/500=51% pred=62/277=22%
* Discontinuous: MWE-based: P=54/62=0.8710 R=54/256=0.2109 F=0.3396

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=499/500=100% pred=186/277=67%
* Multi-token: MWE-based: P=151/186=0.8118 R=151/499=0.3026 F=0.4409
* Single-token: MWE-proportion: gold=1/500=0% pred=91/277=33%
* Single-token: MWE-based: P=0/91=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=364/500=73% pred=160/277=58%
* Seen-in-train: MWE-based: P=149/160=0.9313 R=149/364=0.4093 F=0.5687
* Unseen-in-train: MWE-proportion: gold=136/500=27% pred=117/277=42%
* Unseen-in-train: MWE-based: P=2/117=0.0171 R=2/136=0.0147 F=0.0158

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=266/364=73% pred=82/160=51%
* Variant-of-train: MWE-based: P=76/82=0.9268 R=76/266=0.2857 F=0.4368
* Identical-to-train: MWE-proportion: gold=98/364=27% pred=78/160=49%
* Identical-to-train: MWE-based: P=73/78=0.9359 R=73/98=0.7449 F=0.8295


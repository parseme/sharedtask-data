## Global evaluation
* MWE-based: P=275/372=0.7392 R=275/670=0.4104 F=0.5278
* Tok-based: P=585/751=0.7790 R=585/1416=0.4131 F=0.5399

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=8/670=1% pred=6/372=2%
* IAV: MWE-based: P=0/6=0.0000 R=0/8=0.0000 F=0.0000
* IAV: Tok-based: P=0/7=0.0000 R=0/20=0.0000 F=0.0000
* IRV: MWE-proportion: gold=254/670=38% pred=203/372=55%
* IRV: MWE-based: P=175/203=0.8621 R=175/254=0.6890 F=0.7659
* IRV: Tok-based: P=350/404=0.8663 R=350/508=0.6890 F=0.7675
* LVC.cause: MWE-proportion: gold=52/670=8% pred=3/372=1%
* LVC.cause: MWE-based: P=3/3=1.0000 R=3/52=0.0577 F=0.1091
* LVC.cause: Tok-based: P=6/6=1.0000 R=6/114=0.0526 F=0.1000
* LVC.full: MWE-proportion: gold=274/670=41% pred=110/372=30%
* LVC.full: MWE-based: P=73/110=0.6636 R=73/274=0.2664 F=0.3802
* LVC.full: Tok-based: P=152/219=0.6941 R=152/569=0.2671 F=0.3858
* VID: MWE-proportion: gold=82/670=12% pred=55/372=15%
* VID: MWE-based: P=18/55=0.3273 R=18/82=0.2195 F=0.2628
* VID: Tok-based: P=48/125=0.3840 R=48/205=0.2341 F=0.2909

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=476/670=71% pred=316/372=85%
* Continuous: MWE-based: P=235/316=0.7437 R=235/476=0.4937 F=0.5934
* Discontinuous: MWE-proportion: gold=194/670=29% pred=56/372=15%
* Discontinuous: MWE-based: P=40/56=0.7143 R=40/194=0.2062 F=0.3200

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=670/670=100% pred=350/372=94%
* Multi-token: MWE-based: P=275/350=0.7857 R=275/670=0.4104 F=0.5392

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=450/670=67% pred=279/372=75%
* Seen-in-train: MWE-based: P=251/279=0.8996 R=251/450=0.5578 F=0.6886
* Unseen-in-train: MWE-proportion: gold=220/670=33% pred=93/372=25%
* Unseen-in-train: MWE-based: P=24/93=0.2581 R=24/220=0.1091 F=0.1534

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=160/450=36% pred=56/279=20%
* Variant-of-train: MWE-based: P=50/56=0.8929 R=50/160=0.3125 F=0.4630
* Identical-to-train: MWE-proportion: gold=290/450=64% pred=223/279=80%
* Identical-to-train: MWE-based: P=201/223=0.9013 R=201/290=0.6931 F=0.7836


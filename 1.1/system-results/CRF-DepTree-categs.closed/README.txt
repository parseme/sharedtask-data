XCRF Dependency Tree (with categories)
Erwan Moreau, Ashjan Alsulaimani and Alfredo Maldonado
(Submission #11)

Summary
This system attempts to exploit the dependency tree structure of the sentences in order to identify MWEs. This is achieved by training a tree-structured CRF model which takes into account conditional dependencies between the nodes of the tree (node-parent and possibly node-next sibling). The system is also trained to predict MWE categories.

The system is based on the XCRF software: https://gforge.inria.fr/projects/treecrf

One of the members in the team participated in the creation of the Spanish dataset.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)

## Global evaluation
* MWE-based: P=0/60=0.0000 R=0/502=0.0000 F=0.0000
* Tok-based: P=17/63=0.2698 R=17/1155=0.0147 F=0.0279

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=0/502=0% pred=60/60=100%
* IAV: MWE-based: P=0/60=0.0000 R=0/0=0.0000 F=0.0000
* IAV: Tok-based: P=0/63=0.0000 R=0/0=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=49/502=10% pred=0/60=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/49=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/101=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=211/502=42% pred=0/60=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/211=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/435=0.0000 F=0.0000
* VID: MWE-proportion: gold=182/502=36% pred=0/60=0%
* VID: MWE-based: P=0/0=0.0000 R=0/182=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/467=0.0000 F=0.0000
* VPC.full: MWE-proportion: gold=60/502=12% pred=0/60=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/60=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/152=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=384/502=76% pred=58/60=97%
* Continuous: MWE-based: P=0/58=0.0000 R=0/384=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=118/502=24% pred=2/60=3%
* Discontinuous: MWE-based: P=0/2=0.0000 R=0/118=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/502=100% pred=3/60=5%
* Multi-token: MWE-based: P=0/3=0.0000 R=0/502=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=174/502=35% pred=0/60=0%
* Seen-in-train: MWE-based: P=0/0=0.0000 R=0/174=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=328/502=65% pred=60/60=100%
* Unseen-in-train: MWE-based: P=0/60=0.0000 R=0/328=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=72/174=41% pred=0/0=0%
* Variant-of-train: MWE-based: P=0/0=0.0000 R=0/72=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=102/174=59% pred=0/0=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/102=0.0000 F=0.0000


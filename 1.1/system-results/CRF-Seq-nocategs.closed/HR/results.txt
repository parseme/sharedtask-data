## Global evaluation
* MWE-based: P=115/311=0.3698 R=115/498=0.2309 F=0.2843
* Tok-based: P=379/505=0.7505 R=379/1092=0.3471 F=0.4746

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=188/498=38% pred=311/311=100%
* IAV: MWE-based: P=65/311=0.2090 R=65/188=0.3457 F=0.2605
* IAV: Tok-based: P=219/505=0.4337 R=219/420=0.5214 F=0.4735
* IRV: MWE-proportion: gold=118/498=24% pred=0/311=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/118=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/237=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=31/498=6% pred=0/311=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/31=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/64=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=131/498=26% pred=0/311=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/131=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/293=0.0000 F=0.0000
* VID: MWE-proportion: gold=33/498=7% pred=0/311=0%
* VID: MWE-based: P=0/0=0.0000 R=0/33=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/86=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=291/498=58% pred=277/311=89%
* Continuous: MWE-based: P=101/277=0.3646 R=101/291=0.3471 F=0.3556
* Discontinuous: MWE-proportion: gold=207/498=42% pred=34/311=11%
* Discontinuous: MWE-based: P=14/34=0.4118 R=14/207=0.0676 F=0.1162

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=156/311=50%
* Multi-token: MWE-based: P=115/156=0.7372 R=115/498=0.2309 F=0.3517

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=273/498=55% pred=102/311=33%
* Seen-in-train: MWE-based: P=102/102=1.0000 R=102/273=0.3736 F=0.5440
* Unseen-in-train: MWE-proportion: gold=225/498=45% pred=209/311=67%
* Unseen-in-train: MWE-based: P=13/209=0.0622 R=13/225=0.0578 F=0.0599

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=199/273=73% pred=54/102=53%
* Variant-of-train: MWE-based: P=54/54=1.0000 R=54/199=0.2714 F=0.4269
* Identical-to-train: MWE-proportion: gold=74/273=27% pred=48/102=47%
* Identical-to-train: MWE-based: P=48/48=1.0000 R=48/74=0.6486 F=0.7869


## Global evaluation
* MWE-based: P=320/394=0.8122 R=320/501=0.6387 F=0.7151
* Tok-based: P=750/806=0.9305 R=750/1113=0.6739 F=0.7817

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=0/501=0% pred=394/394=100%
* IAV: MWE-based: P=0/394=0.0000 R=0/0=0.0000 F=0.0000
* IAV: Tok-based: P=0/806=0.0000 R=0/0=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=501/501=100% pred=0/394=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/501=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/1113=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=398/501=79% pred=370/394=94%
* Continuous: MWE-based: P=303/370=0.8189 R=303/398=0.7613 F=0.7891
* Discontinuous: MWE-proportion: gold=103/501=21% pred=24/394=6%
* Discontinuous: MWE-based: P=17/24=0.7083 R=17/103=0.1650 F=0.2677

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=501/501=100% pred=367/394=93%
* Multi-token: MWE-based: P=320/367=0.8719 R=320/501=0.6387 F=0.7373

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=313/501=62% pred=243/394=62%
* Seen-in-train: MWE-based: P=232/243=0.9547 R=232/313=0.7412 F=0.8345
* Unseen-in-train: MWE-proportion: gold=188/501=38% pred=151/394=38%
* Unseen-in-train: MWE-based: P=88/151=0.5828 R=88/188=0.4681 F=0.5192

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=167/313=53% pred=106/243=44%
* Variant-of-train: MWE-based: P=102/106=0.9623 R=102/167=0.6108 F=0.7473
* Identical-to-train: MWE-proportion: gold=146/313=47% pred=137/243=56%
* Identical-to-train: MWE-based: P=130/137=0.9489 R=130/146=0.8904 F=0.9187


## Global evaluation
* MWE-based: P=162/228=0.7105 R=162/506=0.3202 F=0.4414
* Tok-based: P=382/429=0.8904 R=382/1045=0.3656 F=0.5183

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=0/506=0% pred=228/228=100%
* IAV: MWE-based: P=0/228=0.0000 R=0/0=0.0000 F=0.0000
* IAV: Tok-based: P=0/429=0.0000 R=0/0=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=272/506=54% pred=0/228=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/272=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/548=0.0000 F=0.0000
* MVC: MWE-proportion: gold=1/506=0% pred=0/228=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* VID: MWE-proportion: gold=233/506=46% pred=0/228=0%
* VID: MWE-based: P=0/0=0.0000 R=0/233=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/495=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=208/506=41% pred=145/228=64%
* Continuous: MWE-based: P=96/145=0.6621 R=96/208=0.4615 F=0.5439
* Discontinuous: MWE-proportion: gold=298/506=59% pred=83/228=36%
* Discontinuous: MWE-based: P=66/83=0.7952 R=66/298=0.2215 F=0.3465

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/506=99% pred=194/228=85%
* Multi-token: MWE-based: P=162/194=0.8351 R=162/502=0.3227 F=0.4655
* Single-token: MWE-proportion: gold=4/506=1% pred=34/228=15%
* Single-token: MWE-based: P=0/34=0.0000 R=0/4=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=126/506=25% pred=93/228=41%
* Seen-in-train: MWE-based: P=86/93=0.9247 R=86/126=0.6825 F=0.7854
* Unseen-in-train: MWE-proportion: gold=380/506=75% pred=135/228=59%
* Unseen-in-train: MWE-based: P=76/135=0.5630 R=76/380=0.2000 F=0.2951

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=75/126=60% pred=42/93=45%
* Variant-of-train: MWE-based: P=40/42=0.9524 R=40/75=0.5333 F=0.6838
* Identical-to-train: MWE-proportion: gold=51/126=40% pred=51/93=55%
* Identical-to-train: MWE-based: P=46/51=0.9020 R=46/51=0.9020 F=0.9020


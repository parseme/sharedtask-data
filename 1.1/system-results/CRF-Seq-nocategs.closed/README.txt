Sequential CRF (no categories)
Erwan Moreau, Ashjan Alsulaimani and Alfredo Maldonado
(Submission #21)

Summary
This system uses a simple sequential Conditional Random Fields (CRF) model in order to identify MWEs. The CRF model relies solely on the lemma and POS tag. The system does not attempt to recognize categories.

The system is based on the Wapiti software: https://wapiti.limsi.fr

One of the members in the team participated in the creation of the Spanish dataset.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)

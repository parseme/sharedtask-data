## Global evaluation
* MWE-based: P=249/317=0.7855 R=249/515=0.4835 F=0.5986
* Tok-based: P=541/651=0.8310 R=541/1108=0.4883 F=0.6151

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=29/515=6% pred=12/317=4%
* IAV: MWE-based: P=12/12=1.0000 R=12/29=0.4138 F=0.5854
* IAV: Tok-based: P=28/28=1.0000 R=28/64=0.4375 F=0.6087
* IRV: MWE-proportion: gold=249/515=48% pred=198/317=62%
* IRV: MWE-based: P=177/198=0.8939 R=177/249=0.7108 F=0.7919
* IRV: Tok-based: P=357/396=0.9015 R=357/499=0.7154 F=0.7978
* LVC.cause: MWE-proportion: gold=15/515=3% pred=8/317=3%
* LVC.cause: MWE-based: P=0/8=0.0000 R=0/15=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/11=0.0000 R=0/33=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=149/515=29% pred=87/317=27%
* LVC.full: MWE-based: P=47/87=0.5402 R=47/149=0.3154 F=0.3983
* LVC.full: Tok-based: P=100/180=0.5556 R=100/305=0.3279 F=0.4124
* VID: MWE-proportion: gold=73/515=14% pred=12/317=4%
* VID: MWE-based: P=8/12=0.6667 R=8/73=0.1096 F=0.1882
* VID: Tok-based: P=23/36=0.6389 R=23/207=0.1111 F=0.1893

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=361/515=70% pred=262/317=83%
* Continuous: MWE-based: P=218/262=0.8321 R=218/361=0.6039 F=0.6998
* Discontinuous: MWE-proportion: gold=154/515=30% pred=55/317=17%
* Discontinuous: MWE-based: P=31/55=0.5636 R=31/154=0.2013 F=0.2967

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=515/515=100% pred=312/317=98%
* Multi-token: MWE-based: P=249/312=0.7981 R=249/515=0.4835 F=0.6022

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=371/515=72% pred=255/317=80%
* Seen-in-train: MWE-based: P=238/255=0.9333 R=238/371=0.6415 F=0.7604
* Unseen-in-train: MWE-proportion: gold=144/515=28% pred=62/317=20%
* Unseen-in-train: MWE-based: P=11/62=0.1774 R=11/144=0.0764 F=0.1068

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=223/371=60% pred=120/255=47%
* Variant-of-train: MWE-based: P=109/120=0.9083 R=109/223=0.4888 F=0.6356
* Identical-to-train: MWE-proportion: gold=148/371=40% pred=135/255=53%
* Identical-to-train: MWE-based: P=129/135=0.9556 R=129/148=0.8716 F=0.9117


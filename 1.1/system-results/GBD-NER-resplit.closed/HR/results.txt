## Global evaluation
* MWE-based: P=222/605=0.3669 R=222/498=0.4458 F=0.4025
* Tok-based: P=558/1232=0.4529 R=558/1092=0.5110 F=0.4802

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=188/498=38% pred=254/605=42%
* IAV: MWE-based: P=98/254=0.3858 R=98/188=0.5213 F=0.4434
* IAV: Tok-based: P=255/527=0.4839 R=255/420=0.6071 F=0.5385
* IRV: MWE-proportion: gold=118/498=24% pred=119/605=20%
* IRV: MWE-based: P=56/119=0.4706 R=56/118=0.4746 F=0.4726
* IRV: Tok-based: P=112/238=0.4706 R=112/237=0.4726 F=0.4716
* LVC.cause: MWE-proportion: gold=31/498=6% pred=0/605=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/31=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/64=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=131/498=26% pred=232/605=38%
* LVC.full: MWE-based: P=50/232=0.2155 R=50/131=0.3817 F=0.2755
* LVC.full: Tok-based: P=109/467=0.2334 R=109/293=0.3720 F=0.2868
* VID: MWE-proportion: gold=33/498=7% pred=0/605=0%
* VID: MWE-based: P=0/0=0.0000 R=0/33=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/86=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=291/498=58% pred=372/605=61%
* Continuous: MWE-based: P=160/372=0.4301 R=160/291=0.5498 F=0.4827
* Discontinuous: MWE-proportion: gold=207/498=42% pred=233/605=39%
* Discontinuous: MWE-based: P=62/233=0.2661 R=62/207=0.2995 F=0.2818

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=605/605=100%
* Multi-token: MWE-based: P=222/605=0.3669 R=222/498=0.4458 F=0.4025

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=273/498=55% pred=136/605=22%
* Seen-in-train: MWE-based: P=129/136=0.9485 R=129/273=0.4725 F=0.6308
* Unseen-in-train: MWE-proportion: gold=225/498=45% pred=469/605=78%
* Unseen-in-train: MWE-based: P=93/469=0.1983 R=93/225=0.4133 F=0.2680

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=199/273=73% pred=93/136=68%
* Variant-of-train: MWE-based: P=86/93=0.9247 R=86/199=0.4322 F=0.5890
* Identical-to-train: MWE-proportion: gold=74/273=27% pred=43/136=32%
* Identical-to-train: MWE-based: P=43/43=1.0000 R=43/74=0.5811 F=0.7350


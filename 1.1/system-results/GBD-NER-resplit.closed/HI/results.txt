## Global evaluation
* MWE-based: P=374/706=0.5297 R=374/500=0.7480 F=0.6202
* Tok-based: P=809/1446=0.5595 R=809/1104=0.7328 F=0.6345

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=12/500=2% pred=0/706=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/12=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/24=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=320/500=64% pred=505/706=72%
* LVC.full: MWE-based: P=240/505=0.4752 R=240/320=0.7500 F=0.5818
* LVC.full: Tok-based: P=518/1042=0.4971 R=518/682=0.7595 F=0.6009
* MVC: MWE-proportion: gold=130/500=26% pred=201/706=28%
* MVC: MWE-based: P=122/201=0.6070 R=122/130=0.9385 F=0.7372
* MVC: Tok-based: P=244/404=0.6040 R=244/260=0.9385 F=0.7349
* VID: MWE-proportion: gold=38/500=8% pred=0/706=0%
* VID: MWE-based: P=0/0=0.0000 R=0/38=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/138=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=465/500=93% pred=650/706=92%
* Continuous: MWE-based: P=359/650=0.5523 R=359/465=0.7720 F=0.6439
* Discontinuous: MWE-proportion: gold=35/500=7% pred=56/706=8%
* Discontinuous: MWE-based: P=15/56=0.2679 R=15/35=0.4286 F=0.3297

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=706/706=100%
* Multi-token: MWE-based: P=374/706=0.5297 R=374/500=0.7480 F=0.6202

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=286/500=57% pred=267/706=38%
* Seen-in-train: MWE-based: P=248/267=0.9288 R=248/286=0.8671 F=0.8969
* Unseen-in-train: MWE-proportion: gold=214/500=43% pred=439/706=62%
* Unseen-in-train: MWE-based: P=126/439=0.2870 R=126/214=0.5888 F=0.3859

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=140/286=49% pred=127/267=48%
* Variant-of-train: MWE-based: P=111/127=0.8740 R=111/140=0.7929 F=0.8315
* Identical-to-train: MWE-proportion: gold=146/286=51% pred=140/267=52%
* Identical-to-train: MWE-based: P=137/140=0.9786 R=137/146=0.9384 F=0.9580


## Global evaluation
* MWE-based: P=214/2352=0.0910 R=214/500=0.4280 F=0.1501
* Tok-based: P=615/4959=0.1240 R=615/1150=0.5348 F=0.2013

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=64/500=13% pred=553/2352=24%
* IAV: MWE-based: P=12/553=0.0217 R=12/64=0.1875 F=0.0389
* IAV: Tok-based: P=42/1114=0.0377 R=42/148=0.2838 F=0.0666
* IRV: MWE-proportion: gold=121/500=24% pred=340/2352=14%
* IRV: MWE-based: P=74/340=0.2176 R=74/121=0.6116 F=0.3210
* IRV: Tok-based: P=150/680=0.2206 R=150/242=0.6198 F=0.3254
* LVC.cause: MWE-proportion: gold=28/500=6% pred=0/2352=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/63=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=85/500=17% pred=843/2352=36%
* LVC.full: MWE-based: P=20/843=0.0237 R=20/85=0.2353 F=0.0431
* LVC.full: Tok-based: P=47/1691=0.0278 R=47/176=0.2670 F=0.0503
* MVC: MWE-proportion: gold=106/500=21% pred=455/2352=19%
* MVC: MWE-based: P=75/455=0.1648 R=75/106=0.7075 F=0.2674
* MVC: Tok-based: P=183/985=0.1858 R=183/249=0.7349 F=0.2966
* VID: MWE-proportion: gold=95/500=19% pred=161/2352=7%
* VID: MWE-based: P=11/161=0.0683 R=11/95=0.1158 F=0.0859
* VID: Tok-based: P=39/489=0.0798 R=39/270=0.1444 F=0.1028
* VPC.full: MWE-proportion: gold=1/500=0% pred=0/2352=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=359/500=72% pred=1491/2352=63%
* Continuous: MWE-based: P=184/1491=0.1234 R=184/359=0.5125 F=0.1989
* Discontinuous: MWE-proportion: gold=141/500=28% pred=861/2352=37%
* Discontinuous: MWE-based: P=30/861=0.0348 R=30/141=0.2128 F=0.0599

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=2352/2352=100%
* Multi-token: MWE-based: P=214/2352=0.0910 R=214/500=0.4280 F=0.1501

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=279/500=56% pred=324/2352=14%
* Seen-in-train: MWE-based: P=129/324=0.3981 R=129/279=0.4624 F=0.4279
* Unseen-in-train: MWE-proportion: gold=221/500=44% pred=2028/2352=86%
* Unseen-in-train: MWE-based: P=85/2028=0.0419 R=85/221=0.3846 F=0.0756

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=145/279=52% pred=159/324=49%
* Variant-of-train: MWE-based: P=60/159=0.3774 R=60/145=0.4138 F=0.3947
* Identical-to-train: MWE-proportion: gold=134/279=48% pred=165/324=51%
* Identical-to-train: MWE-based: P=69/165=0.4182 R=69/134=0.5149 F=0.4615


## Global evaluation
* MWE-based: P=296/1399=0.2116 R=296/500=0.5920 F=0.3117
* Tok-based: P=715/2920=0.2449 R=715/1113=0.6424 F=0.3546

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=101/500=20% pred=301/1399=22%
* IAV: MWE-based: P=49/301=0.1628 R=49/101=0.4851 F=0.2438
* IAV: Tok-based: P=106/610=0.1738 R=106/221=0.4796 F=0.2551
* IRV: MWE-proportion: gold=245/500=49% pred=489/1399=35%
* IRV: MWE-based: P=210/489=0.4294 R=210/245=0.8571 F=0.5722
* IRV: Tok-based: P=423/978=0.4325 R=423/490=0.8633 F=0.5763
* LVC.cause: MWE-proportion: gold=13/500=3% pred=0/1399=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/13=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/31=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=35/500=7% pred=107/1399=8%
* LVC.full: MWE-based: P=9/107=0.0841 R=9/35=0.2571 F=0.1268
* LVC.full: Tok-based: P=21/214=0.0981 R=21/75=0.2800 F=0.1453
* VID: MWE-proportion: gold=106/500=21% pred=502/1399=36%
* VID: MWE-based: P=20/502=0.0398 R=20/106=0.1887 F=0.0658
* VID: Tok-based: P=73/1118=0.0653 R=73/296=0.2466 F=0.1033

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=244/500=49% pred=830/1399=59%
* Continuous: MWE-based: P=150/830=0.1807 R=150/244=0.6148 F=0.2793
* Discontinuous: MWE-proportion: gold=256/500=51% pred=569/1399=41%
* Discontinuous: MWE-based: P=146/569=0.2566 R=146/256=0.5703 F=0.3539

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=499/500=100% pred=1399/1399=100%
* Multi-token: MWE-based: P=296/1399=0.2116 R=296/499=0.5932 F=0.3119
* Single-token: MWE-proportion: gold=1/500=0% pred=0/1399=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=364/500=73% pred=304/1399=22%
* Seen-in-train: MWE-based: P=237/304=0.7796 R=237/364=0.6511 F=0.7096
* Unseen-in-train: MWE-proportion: gold=136/500=27% pred=1095/1399=78%
* Unseen-in-train: MWE-based: P=59/1095=0.0539 R=59/136=0.4338 F=0.0959

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=266/364=73% pred=218/304=72%
* Variant-of-train: MWE-based: P=161/218=0.7385 R=161/266=0.6053 F=0.6653
* Identical-to-train: MWE-proportion: gold=98/364=27% pred=86/304=28%
* Identical-to-train: MWE-based: P=76/86=0.8837 R=76/98=0.7755 F=0.8261


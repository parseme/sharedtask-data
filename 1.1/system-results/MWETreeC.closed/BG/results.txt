## Global evaluation
* MWE-based: P=0/851=0.0000 R=0/670=0.0000 F=0.0000
* Tok-based: P=43/851=0.0505 R=43/1416=0.0304 F=0.0379

## Per-category evaluation (partition of Global)
* <unlabeled>: MWE-proportion: gold=0/670=0% pred=11/851=1%
* <unlabeled>: MWE-based: P=0/11=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/11=0.0000 R=0/0=0.0000 F=0.0000
* IAV: MWE-proportion: gold=8/670=1% pred=4/851=0%
* IAV: MWE-based: P=0/4=0.0000 R=0/8=0.0000 F=0.0000
* IAV: Tok-based: P=1/4=0.2500 R=1/20=0.0500 F=0.0833
* IRV: MWE-proportion: gold=254/670=38% pred=515/851=61%
* IRV: MWE-based: P=0/515=0.0000 R=0/254=0.0000 F=0.0000
* IRV: Tok-based: P=10/515=0.0194 R=10/508=0.0197 F=0.0196
* LVC.cause: MWE-proportion: gold=52/670=8% pred=14/851=2%
* LVC.cause: MWE-based: P=0/14=0.0000 R=0/52=0.0000 F=0.0000
* LVC.cause: Tok-based: P=1/14=0.0714 R=1/114=0.0088 F=0.0156
* LVC.full: MWE-proportion: gold=274/670=41% pred=215/851=25%
* LVC.full: MWE-based: P=0/215=0.0000 R=0/274=0.0000 F=0.0000
* LVC.full: Tok-based: P=11/215=0.0512 R=11/569=0.0193 F=0.0281
* VID: MWE-proportion: gold=82/670=12% pred=92/851=11%
* VID: MWE-based: P=0/92=0.0000 R=0/82=0.0000 F=0.0000
* VID: Tok-based: P=2/92=0.0217 R=2/205=0.0098 F=0.0135

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=476/670=71% pred=851/851=100%
* Continuous: MWE-based: P=0/851=0.0000 R=0/476=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=194/670=29% pred=0/851=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/194=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=670/670=100% pred=0/851=0%
* Multi-token: MWE-based: P=0/0=0.0000 R=0/670=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=450/670=67% pred=0/851=0%
* Seen-in-train: MWE-based: P=0/0=0.0000 R=0/450=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=220/670=33% pred=851/851=100%
* Unseen-in-train: MWE-based: P=0/851=0.0000 R=0/220=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=160/450=36% pred=0/0=0%
* Variant-of-train: MWE-based: P=0/0=0.0000 R=0/160=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=290/450=64% pred=0/0=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/290=0.0000 F=0.0000


## Global evaluation
* MWE-based: P=0/894=0.0000 R=0/498=0.0000 F=0.0000
* Tok-based: P=310/894=0.3468 R=310/1171=0.2647 F=0.3002

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=108/498=22% pred=136/894=15%
* IRV: MWE-based: P=0/136=0.0000 R=0/108=0.0000 F=0.0000
* IRV: Tok-based: P=62/136=0.4559 R=62/216=0.2870 F=0.3523
* LVC.cause: MWE-proportion: gold=14/498=3% pred=6/894=1%
* LVC.cause: MWE-based: P=0/6=0.0000 R=0/14=0.0000 F=0.0000
* LVC.cause: Tok-based: P=1/6=0.1667 R=1/28=0.0357 F=0.0588
* LVC.full: MWE-proportion: gold=160/498=32% pred=223/894=25%
* LVC.full: MWE-based: P=0/223=0.0000 R=0/160=0.0000 F=0.0000
* LVC.full: Tok-based: P=74/223=0.3318 R=74/333=0.2222 F=0.2662
* MVC: MWE-proportion: gold=4/498=1% pred=0/894=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* VID: MWE-proportion: gold=212/498=43% pred=533/894=60%
* VID: MWE-based: P=0/533=0.0000 R=0/212=0.0000 F=0.0000
* VID: Tok-based: P=140/533=0.2627 R=140/586=0.2389 F=0.2502

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=281/498=56% pred=894/894=100%
* Continuous: MWE-based: P=0/894=0.0000 R=0/281=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=217/498=44% pred=0/894=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/217=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=0/894=0%
* Multi-token: MWE-based: P=0/0=0.0000 R=0/498=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=251/498=50% pred=0/894=0%
* Seen-in-train: MWE-based: P=0/0=0.0000 R=0/251=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=247/498=50% pred=894/894=100%
* Unseen-in-train: MWE-based: P=0/894=0.0000 R=0/247=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=126/251=50% pred=0/0=0%
* Variant-of-train: MWE-based: P=0/0=0.0000 R=0/126=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=125/251=50% pred=0/0=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/125=0.0000 F=0.0000


## Global evaluation
* MWE-based: P=0/772=0.0000 R=0/515=0.0000 F=0.0000
* Tok-based: P=338/772=0.4378 R=338/1108=0.3051 F=0.3596

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=29/515=6% pred=20/772=3%
* IAV: MWE-based: P=0/20=0.0000 R=0/29=0.0000 F=0.0000
* IAV: Tok-based: P=17/20=0.8500 R=17/64=0.2656 F=0.4048
* IRV: MWE-proportion: gold=249/515=48% pred=565/772=73%
* IRV: MWE-based: P=0/565=0.0000 R=0/249=0.0000 F=0.0000
* IRV: Tok-based: P=247/565=0.4372 R=247/499=0.4950 F=0.4643
* LVC.cause: MWE-proportion: gold=15/515=3% pred=15/772=2%
* LVC.cause: MWE-based: P=0/15=0.0000 R=0/15=0.0000 F=0.0000
* LVC.cause: Tok-based: P=3/15=0.2000 R=3/33=0.0909 F=0.1250
* LVC.full: MWE-proportion: gold=149/515=29% pred=140/772=18%
* LVC.full: MWE-based: P=0/140=0.0000 R=0/149=0.0000 F=0.0000
* LVC.full: Tok-based: P=46/140=0.3286 R=46/305=0.1508 F=0.2067
* VID: MWE-proportion: gold=73/515=14% pred=36/772=5%
* VID: MWE-based: P=0/36=0.0000 R=0/73=0.0000 F=0.0000
* VID: Tok-based: P=8/36=0.2222 R=8/207=0.0386 F=0.0658

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=361/515=70% pred=772/772=100%
* Continuous: MWE-based: P=0/772=0.0000 R=0/361=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=154/515=30% pred=0/772=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/154=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=515/515=100% pred=0/772=0%
* Multi-token: MWE-based: P=0/0=0.0000 R=0/515=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=371/515=72% pred=0/772=0%
* Seen-in-train: MWE-based: P=0/0=0.0000 R=0/371=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=144/515=28% pred=772/772=100%
* Unseen-in-train: MWE-based: P=0/772=0.0000 R=0/144=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=223/371=60% pred=0/0=0%
* Variant-of-train: MWE-based: P=0/0=0.0000 R=0/223=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=148/371=40% pred=0/0=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/148=0.0000 F=0.0000


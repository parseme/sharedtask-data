Milos
Luka Nerima, Vasiliki Foufi and Eric Wehrli
(Submission #8)

Summary
This system is based on a linguistic, rule based syntactic parser and on a collocation extraction module. The ressources used are the monolingual dictionaries of the systems. Vasiliki Foufi was involved in the creation of the datasets for Greek.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	OPEN
 	
Languages:  	
German (DE)
Greek (EL)
English (EN)
French (FR)

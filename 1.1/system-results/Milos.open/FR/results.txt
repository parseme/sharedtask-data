## Global evaluation
* MWE-based: P=220/647=0.3400 R=220/498=0.4418 F=0.3843
* Tok-based: P=626/1270=0.4929 R=626/1171=0.5346 F=0.5129

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=108/498=22% pred=204/647=32%
* IRV: MWE-based: P=87/204=0.4265 R=87/108=0.8056 F=0.5577
* IRV: Tok-based: P=176/408=0.4314 R=176/216=0.8148 F=0.5641
* LVC.cause: MWE-proportion: gold=14/498=3% pred=0/647=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/14=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=160/498=32% pred=255/647=39%
* LVC.full: MWE-based: P=69/255=0.2706 R=69/160=0.4313 F=0.3325
* LVC.full: Tok-based: P=153/481=0.3181 R=153/333=0.4595 F=0.3759
* MVC: MWE-proportion: gold=4/498=1% pred=0/647=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* <unlabeled>: MWE-proportion: gold=0/498=0% pred=7/647=1%
* <unlabeled>: MWE-based: P=0/7=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/7=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=212/498=43% pred=181/647=28%
* VID: MWE-based: P=27/181=0.1492 R=27/212=0.1274 F=0.1374
* VID: Tok-based: P=146/374=0.3904 R=146/586=0.2491 F=0.3042

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=281/498=56% pred=354/647=55%
* Continuous: MWE-based: P=132/354=0.3729 R=132/281=0.4698 F=0.4157
* Discontinuous: MWE-proportion: gold=217/498=44% pred=293/647=45%
* Discontinuous: MWE-based: P=88/293=0.3003 R=88/217=0.4055 F=0.3451

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=591/647=91%
* Multi-token: MWE-based: P=220/591=0.3723 R=220/498=0.4418 F=0.4040

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=251/498=50% pred=160/647=25%
* Seen-in-train: MWE-based: P=126/160=0.7875 R=126/251=0.5020 F=0.6131
* Unseen-in-train: MWE-proportion: gold=247/498=50% pred=487/647=75%
* Unseen-in-train: MWE-based: P=94/487=0.1930 R=94/247=0.3806 F=0.2561

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=126/251=50% pred=76/160=48%
* Variant-of-train: MWE-based: P=57/76=0.7500 R=57/126=0.4524 F=0.5644
* Identical-to-train: MWE-proportion: gold=125/251=50% pred=84/160=52%
* Identical-to-train: MWE-based: P=69/84=0.8214 R=69/125=0.5520 F=0.6603

